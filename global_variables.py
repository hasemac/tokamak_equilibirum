device_name = "quest"
root_dir = "."
#root_dir = "c:\\home\\code\\tokamak_equilibirum\\"

import sys, os
import numpy as np
import pandas as pd
import glob
path = os.path.join(root_dir, 'device', device_name)
sys.path.append(path)
from dev_params import equi_params
import sub.sub_func as ssf
import copy

class gparam(equi_params):
    
    device_name = device_name
    root_dir = root_dir
    path_dev = path
    
    nr, nz = None, None
    r_pos, z_pos = [], []
    
    cnr, cnz = None, None
    cr_pos, cz_pos = [], []

    image_frame = None # collection of lines
    vessel = None # dm_mat

    def __init__(self):
        # conversion from real position to num is below
        # round((r-r_rmin)/del_r)
        
        self.set_mesh_info()        
        self.set_image_info()
        self.set_vessel_info()
        
    def __setattr__(self, name, value):
        if name in self.__dict__:
            # 定義されたら例外出す
            raise TypeError(f"Caution! Can't rebind const ({name})")
        self.__dict__[name] = value

    def get_dmat_fine(self):
        a = {
            "rmin": self.r_min,
            "rmax": self.r_max,
            "dr": self.del_r,
            "zmin": self.z_min,
            "zmax": self.z_max,
            "dz": self.del_z,
        }
        return a

    def get_dmat_coarse(self):
        a = {
            "rmin": self.cr_min,
            "rmax": self.cr_max,
            "dr": self.cdel_r,
            "zmin": self.cz_min,
            "zmax": self.cz_max,
            "dz": self.cdel_z,
        }
        return a

    def set_mesh_info(self):
        self.r_pos = self.get_arith_seq(self.r_min, self.r_max, self.del_r)
        self.nr = len(self.r_pos)

        self.z_pos = self.get_arith_seq(self.z_min, self.z_max, self.del_z)
        self.nz = len(self.z_pos)

        self.cr_pos = self.get_arith_seq(self.cr_min, self.cr_max, self.cdel_r)
        self.cnr = len(self.cr_pos)

        self.cz_pos = self.get_arith_seq(self.cz_min, self.cz_max, self.cdel_z)
        self.cnz = len(self.cz_pos)
                
    def set_image_info(self):
        xl, xr = self.image_x0, self.image_x0 + self.image_sizex
        yt, yb = self.image_y0, self.image_y0 - self.image_sizey
        fr = [
            dict(type='line', x0=xl, y0=yt, x1=xr, y1=yt),
            dict(type='line', x0=xr, y0=yt, x1=xr, y1=yb),
            dict(type='line', x0=xr, y0=yb, x1=xl, y1=yb),
            dict(type='line', x0=xl, y0=yb, x1=xl, y1=yt),
        ]
        if self.image_type == 'lines':
            df = pd.read_csv(self.image_path, comment='#')
            fr2 = [dict(type='line', x0=e[0], y0=e[1], x1=e[2], y1=e[3]) for e in df.values]
            self.image_frame = fr + fr2
        else:
            self.image_frame = fr

    def set_vessel_info(self):
        path = os.path.join(self.root_dir, 'device', self.device_name, 'vessel')
        
        # ディレクトリ有無の確認
        if not os.path.exists(path):
            os.makedirs(path)
                    
        # ファイル有無の確認
        file = os.path.join(path, 'vessel.npy')
        file_txt = os.path.join(path, 'vessel.txt')
        if self.coil_dir != 'coils':
            file = os.path.join(path, f'vessel_{self.coil_dir}.npy')
            file_txt = os.path.join(path, f'vessel_{self.coil_dir}.txt')
            
        # 二つのファイルが存在していて、数値が一致の場合はロード
        if os.path.exists(file) and os.path.exists(file_txt):
            txt_dat = np.loadtxt(file_txt)
            if (txt_dat == self.vessel_points).all():
                self.vessel = np.load(file, allow_pickle=True).item()
                return
        
        # 二つのファイルあれば削除
        if os.path.exists(file_txt):
            os.remove(file_txt)
        if os.path.exists(file):
            os.remove(file)
        
        # ファイルが存在しないので作成する。
        print('making vessel matrix.')
        
        # textデータの出力
        # 出力するとき値を丸め込まないこと
        # 丸め込むと比較時に一致しなくなる。
        #np.savetxt(file_txt, self.vessel_points,  fmt='%.4e')
        np.savetxt(file_txt, self.vessel_points)
                
        # numpyデータの出力
        d_mat = self.get_dmat_fine()
        d_mat['matrix'] = np.zeros((self.nz, self.nr))
        pts = self.vessel_points
        for e in range(len(pts)-1):
            ssf.draw_line(d_mat, pts[e], pts[e+1])
        mat_line = copy.deepcopy(d_mat['matrix']) # 輪郭を退避
        
        # 真空容器内の中心点からPaintをする。
        dmat = ssf.draw_paint(d_mat, self.vessel_center)
        # 真空容器の線上を真空容器の内、外のどちらにするか。
        # 線上を真空容器の中にする場合は下の行をコメントアウト
        dmat['matrix'] -= mat_line # 線上は真空容器外の場合
        self.vessel = dmat 
        
        np.save(file, self.vessel)

    def read_cal_cond(self, file_name):
        fipa = os.path.join(self.path_dev, file_name)
        a = eval(open(fipa, 'r', newline='').read())
        return a

    def get_coil_names(self):
        path = os.path.join(self.root_dir, 'device', self.device_name, 'coils', 'data_npy', '*.npy')
        file_names = [os.path.basename(e) for e in glob.glob(path)]
        coil_names = [os.path.splitext(e)[0] for e in file_names]
        return coil_names
    
    def get_coarse_grid_num(self, r, z):
        """r, zの位置から粗いメッシュの番号を計算する。
        Args:
            r (float): [m] r位置
            z (float): [m] z位置
            
        Returns:
            (int, int): (ir, iz) 粗いメッシュの番号
        """
        rmin, dr = self.cr_min, self.cdel_r
        zmin, dz = self.cz_min, self.cdel_z
        fr = (r - rmin)/dr
        ir = int(np.floor(fr))
        fr -= ir
        if fr > 0.5:
            ir += 1
        
        fz = (z - zmin)/dz
        iz = int(np.floor(fz))
        fz -= iz
        if fz > 0.5:
            iz += 1
        
        return (ir, iz)

    # 等差数列の作成
    def get_arith_seq(self, min, max, delta):
        """arithmetic sequence
        maxを含む等差数列を計算する。

        Args:
            min (float): min
            max (float): max
            delta (float): delta

        Returns:
            array of float: arithmetic sequence
        """
        # ex. np.arange(0.1, 1.8+0.02/2, 0.02)とすると最後の要素は
        # 1.8000000000000005となって、僅かに1.8より大きくなる。
        # このmin, max, drは、メートル単位なので、基本的に小数点３桁で丸め込みをして良い。
        # ex. round(3.45678, 3)は、3.457を返す。
        ar = np.arange(min, max+delta/2, delta)
        ar = [round(e, 3) for e in ar] # 小数３桁で丸め込み
        ar = np.array(ar)
        if ar[-1] > max:
            ar = ar[:-1]
        return ar

    # プラズマ計算行列用のz位置作成
    def get_z_pos_pmat(self):
        # プラズマ計算行列用のz位置作成
        # self.cz_minを中心に、対称のz位置を作成する。
        
        # 最初にcdel_zの等差数列を所望の長さで作成
        b = [self.cdel_z*i for i in range(len(self.cz_pos)-1)]
        b = np.array(b)
        # 最後の要素がcz_minよりcdel_zだけ小さい値になるように全体を調節
        b = b - b[-1] + self.cz_min - self.cdel_z
        # 行列を連結して完成
        b = np.concatenate([b, self.cz_pos])
        # この時要素数は 2*cnz - 1 になる。
        return b

# import sys
# sys.modules["equilibrium_global"]=_const()
