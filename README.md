# Tokamak equilibrium code

## Overview

This repository provides free-boundary and fix-boundary equilibrium calculation code applicable to general tokamak devices.

Contents:

- [Tokamak equilibrium code](#tokamak-equilibrium-code)
  - [Overview](#overview)
  - [New features](#new-features)
  - [Features of this equilibrium calculation code](#features-of-this-equilibrium-calculation-code)
  - [Other documents](#other-documents)
  - [Getting started](#getting-started)
  - [Examples of calculation result](#examples-of-calculation-result)
  - [Support](#support)

## New features

- (Dec. 3, 2024)  
  - Equilibrium calculations for fixed boundaries can now be performed.
- (Mar. 14, 2024)  
  - Calculation conditions can now be specified in detail (conv_judge_val, bad_step_max, etc.).
- (Mar. 12, 2024)
  - The Strike points can now be determined more accurately.
  - Plasma boundaries are now output accurately.
  - The default contour diagram now includes plasma boundaries.

## Features of this equilibrium calculation code

- The environment is easy to set up and can be executed immediately.
- Easily apply the calculation code to other tokamak devices.
- Equilibrium calculations for fixed and free boundaries can be performed.
- Supports output to [g eqdsk format](https://w3.pppl.gov/ntcc/TORAY/G_EQDSK.pdf), format for equilibrium calculation results also used in EFIT.
- Supports output of calculation results to relational database.
- Contains complementary analysis tools.
  - [Magnetics Calculation](doc/magnetics_cal.md)
  - [Tracing magnetic field lines](doc/tracing_mag_lines.md)
  - [Tracing guiding center orbit](doc/tracing_guiding_center_orbit.md)

## Other documents

- Equilibrium code
  - [Calculation condition](doc/cal_cond.md)
  - [Execution of calculation](doc/exec_cal.md)
  - [Input and output parameters](doc/1_params.md)
  - [How to change resolution or calculation area](doc/change_reso_area.md)
  - [How to apply this code to new tokamak](doc/new_tokamak.md)
  - [Tips](doc/tips.md) (g-file, database)
- Complementary analysis tools
  - [Magnetics Calculation](doc/magnetics_cal.md)
  - [Tracing magnetic field lines](doc/tracing_mag_lines.md)
  - [Tracing guiding center orbit](doc/tracing_guiding_center_orbit.md)
- Derivation of general equation
  - [Electromagnetism in cylindrical coordinates](doc/magnetics_en.md)
  - [Grad-Shafranov equation](doc/grad_shafranov_eq.md)
  - [Procedure of equilibrium calculation](doc/equilibrium_en.md)
  - [Equilibrium with fixed boundary](doc/equilibrium_with_surface_en.md)
  - [Definition of parameters](doc/def_of_params.md)
    - [Inductance](doc/def_of_inductance.md)
    - [beta](doc/def_of_beta.md)
  
- Device
  - [Introducig QUEST](doc/introducing_quest.md)

## Getting started

1. Clone this project using VS code etc.
1. Go to the cloned directory, and execute below to install required modules.

   windows
  
   ```shell
   > python -m pip install -r requirements.txt
   ```

   Mac

   ```shell
   > python3 -m pip install -r requirements.txt
   ```

   - Be aware of whether or not you use a virtual environment.  
   - When using it, execute the above command under a virtual environment.

1. Check the device name to calculate.  
   The first line of global_variable.py in the root directory.

   ```python
   device_name = "quest"
   root_dir = "."
   ```

1. Copy the matrix data to to your repository.  
   Matrix data can be downloaded from the link below.  
   Copy them to ./device/(device_name)  
   <https://archive.iii.kyushu-u.ac.jp/public/LSIfwKJJ2-DahDRTPVXYybO8RYcyQjtueCvJ_vEm2KgW>  
   If you do not find matrix data for your device, proceed to the next step.
   Although it will take some time, the matrix data will be created automatically in the next step.  

1. Execute `equalibrium.ipynb` in the root directory in order from the top as an example.  

## Examples of calculation result

QUEST  
![quest](doc/images/result_quest2.png)

Dark green: Last Closed Flux Surface  
Light green: Fixed boundary  
Orange: 95% poloidal flux  

PLATO  
![plato](doc/images/result_plato.png)

<!-- PLATO
You can check the calculation result with contour map or heat map.

```python:
import sub.plot as pl
pl.d_contour(cond['flux'])
```

![flux](doc/flux.png)

```python:
import sub.plot as pl
pl.d_heatmap(cond['domain'])
```

![domain](doc/domain.png) -->

## Support

We are accepting questions at any time by e-mail (hasegawa(atm)triam.kyushu-u.ac.jp).
