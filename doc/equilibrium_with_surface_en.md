# Equilibrium with fixed boundary

The boundary may be the last closed flux surface (LCFS), a diverter leg, or a 95% poloidal flux. The PF coil currents are adjusted so that the magnetic flux at a specific location reaches a specific value.  
The calculation procedure is almost the same as for the free boundary case. A new step is added to adjust the PF coil current for each iteration.  

## Procedure

### Preparation in advance

1. Determine specific points, such as magnetic surfaces or diverter legs.  
2. Calculate the magnetic flux at the specific points when a unit current is applied to each coil.

### Operations per iteration

1. Perform one iteration to obtain magnetic flux such as the LCFS and 95% poloidal flux position.
2. Obtain magnetic flux (plasma-derived + coil-derived) at each specific points.
3. Determin the increase/decrease in each coil current from the difference between the magnetic flux at the LCFS, etc. and the magnetic flux at each specific point.
4. Return to 1. for the new coil current.

### Supplementary explanation

(Preparation in advance: 2)

It means to create a matrix in which the magnetic flux at each specific point can be calculated with the given current for each coil.

```math
\begin{pmatrix}
\phi_{r0} \\
\phi_{r1} \\
\vdots \\
\phi_{rm} \\
\end{pmatrix}
=
\Phi_{mn}
\begin{pmatrix}
i_{c0} \\
i_{c1} \\
\vdots \\
i_{cn} \\
\end{pmatrix}
```

$\phi_{rk}$ : Magnetic flux of a specific point $rk$.
$i_{cl}$ : Current of coil $cl$.

(Operations per iteration: 3)

$\phi_{bj}$ : Magnetic flux at LCFS, 95% position, etc.
$\phi_{rk}'$ : Magnetic flux at specific points

```math
\boldsymbol{f}'=
\begin{pmatrix}
\phi_{r0}' - \phi_{b0} \\
\phi_{r1}' - \phi_{b1} \\
\vdots \\
\phi_{rm}' - \phi_{bm}\\
\end{pmatrix}
```

If all specific points are on the LCFS,
$\phi_{b0}=\phi_{b1}=\cdots=\phi_{bm}$

We can adjust the coil current $\delta \boldsymbol{i}$ so that $|\boldsymbol{f}'|^2$ is the smallest.  
This is obtained from the following relational equation.  

```math
\Phi_{mn}^T \Phi_{mn} \delta \boldsymbol{i} = \Phi_{mn}^T \boldsymbol{f}'
```

The weighting factors, if any, are as follows

```math
\Phi_{mn}^{T}W\Phi_{mn}\boldsymbol{i}=(W\Phi_{mn})^{T}\boldsymbol{f}'
```

The weighting matrix is the square of the weighting factor as shown below.

```math
W = 
\begin{pmatrix}
w_{1}^{2} & \cdots & 0 & \cdots & 0\\
\vdots & \ddots & & & \vdots \\
0 & & w_{i}^{2} & & 0 \\
\vdots & & & \ddots & \vdots \\
0 & \cdots & 0 & \cdots & w_{m}^{2}
\end{pmatrix}
```

## Convergence judgment

It is difficult to use the rate of variation of the coil current or the flux value to see its convergence in an equilibrium calculation that adjusts the coil current. For example, the coil current may converge to zero, in which case the rate of variation may be very large.  
Therefore, no new criteria should be used, but only the conventional change in plasma current density distribution should be used to make a judgment.
