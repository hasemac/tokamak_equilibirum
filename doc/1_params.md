# Explanation of input and output parameters

## Input parameters

| name           | subname      | unit | type    | Necessary               | description                                                                         |
|----------------|--------------|------|---------|-------------------------|-------------------------------------------------------------------------------------|
| bad_step_max   |              |      | int     | optional (4)            | the calculation ends when bad_step exceeds this value.                              |
| br_pos         |              |      | dict    | optional                | positions to calculate br                                                           |
|                | user_defined | m    | tuple   |                         |                                                                                     |
| bz_pos         |              |      | dict    | optional                | positions to calculate bz                                                           |
|                | user_defined | m    | tuple   |                         |                                                                                     |
| constraints    |              |      | dict    | optional                | constraints for pressure, flux, br, and bz                                          |
|                | user_defined |      |         |                         |                                                                                     |
| conv_judge_val |              |      | float   | optional (0.05)         | A value for judging convergence, convegence when error value become less than this. |
| cur_ip         |              |      | dict    |                         | setting for plasma current                                                          |
|                | ip           | A    | float   |                         | plasma current                                                                      |
|                | r0           | m    | float   |                         | initial major radius                                                                |
|                | z0           | m    | float   |                         | initial z position                                                                  |
|                | radius       | m    | float   |                         | initial minor radius                                                                |
|                | degree       |      | float   | optional (2: parabolic) | initial current profile(ex. 2: parabolic)                                           |
| cur_pf         |              |      |         |                         | setting for poloidal coils                                                          |
|                | PF coil name | A    | float   |                         | coil current                                                                        |
| cur_tf         |              |      |         |                         | setting for toroidal coils                                                          |
|                | tf           |      | float   |                         | power supply current of tf                                                          |
|                | turn         |      | int     |                         | turns of toroidal coil                                                              |
|                | rewind       |      | boolean | optional (False)        | rewinding (True: consider, False: not consider)                                     |
| fix_pos        |              |      | boolean | optional (False)        | flag to fix magnetic axis at initial plasma profile (r0, z0). Default is False.     |
| fl_pos         |              |      |         | optional                | position to calculate flux                                                          |
|                | user_defined | m    | tuple   |                         |                                                                                     |
| num_di2        |              |      | int     | optional (1)            | number of terms in the polynominal for ff'                                          |
| num_dpr        |              |      | int     | optional (1)            | number of terms in the polynominal for p'                                           |

## Output parameters

| name                           | subname | unit      | type           | description                                                             |
|--------------------------------|---------|-----------|----------------|-------------------------------------------------------------------------|
| aspect_ratio                   |         |           | float          | aspect ratio                                                            |
| axis_ir                        |         |           | int            | r-grid number of magnetic axis                                          |
| axis_iz                        |         |           | int            | z-grid number of magnetic axis                                          |
| axis_r                         |         | m         | float          | r of magnetic axis                                                      |
| axis_z                         |         | m         | float          | z of magnetic axis                                                      |
| b_theta_jt_square              |         |           |                |                                                                         |
| bad_step                       |         |           |                | Number of times the evaluation function value became bad.               |
| beta_normalized                |         |           | float          | normalized beta                                                         |
| beta_poloidal                  |         |           | float          | poloidal beta                                                           |
| beta_toroidal                  |         |           | float          | toroidal beta                                                           |
| boundary_plasma                |         |           | array          | boundary of plasma, [(r0, z0), (r1, z1), , , ]                          |
| boundary_x                     |         |           | array          | boundary of plasma and private region, [(r0, z0), (r1, z1), , , ]       |
| br                             |         | T, Wb/m^2 | matrix         | total br (br_plasma + br_coil)                                          |
| br_coil                        |         | T, Wb/m^2 | matrix         | br due to PF coils                                                      |
| br_plasma                      |         | T, Wb/m^2 | matrix         | br due to plasma current                                                |
| bt                             |         | T, Wb/m^2 | matrix         | total bt (bt_plasma + bt_coil)                                          |
| bt_coil                        |         | T, Wb/m^2 | matrix         | bt due to TF coils                                                      |
| bt_plasma                      |         | T, Wb/m^2 | matrix         | bt due to plasma current                                                |
| bz                             |         | T, Wb/m^2 | matrix         | total bz (bz_plasma + bz_coil)                                          |
| bz_coil                        |         | T, Wb/m^2 | matrix         | bz due to PF coils                                                      |
| bz_plazma                      |         | T, Wb/m^2 | matrix         | bz due to plasma current                                                |
| cal_result                     |         |           | int            | calculation result (1: success, -1: failure)                            |
| conf_div                       |         |           | int            | plasma configuration (1: divertor, 0: limiter)                          |
| cross_section                  |         | m^2       | float          | poloidal cross section                                                  |
| decay_index_on_axis            |         |           | float          | decay index on axis                                                     |
| diff_i2                        |         |           |                |                                                                         |
| diff_i2_norm                   |         |           |                |                                                                         |
| diff_pre                       |         |           |                |                                                                         |
| diff_pre_norm                  |         |           |                |                                                                         |
| domain                         |         |           | matrix         | plasma domain (1:inside, 0:outside)                                     |
| elongation                     |         |           | float          | elongation of plasma                                                    |
| error                          |         |           | array          | history of evaluation function value                                    |
| error_messages                 |         |           | str            | error message                                                           |
| f_axis                         |         | Vsec, Wb  | float          | flux of magnetic axis                                                   |
| f_surf                         |         | Vsec, Wb  | float          | flux of magnetic surface                                                |
| fix_cur                        |         |           | boolean (True) | flag to fix plasma current. False when no IP constraint.                |
| fl_val                         |         | Vsec, Wb  | float          |                                                                         |
| flux                           |         | Vsec, Wb  | matrix         | total flux (flux_plasma + flux_coil)                                    |
| flux_coil                      |         | Vsec, Wb  | matrix         | magnetic flux due to PF coils                                           |
| flux_plasma                    |         | Vsec, Wb  | matrix         | magnetic flux due to plasma current                                     |
| flux_normalized                |         | Vsec, Wb  | matrix         | normalized flux (0:axis, 1:surface)                                     |
| inductance_internal            |         | H         | float          | internal inductance, Li=Fi/Ip                                           |
| inductance_internal_btheta     |         | H         | float          | internal inductance, Li=<Bth^2>V/(m0 Ip^2)                              |
| inductance_internal_normalized |         | H         | float          | normalized internal inductance, li                                      |
| inductance_self                |         | H         | float          | self inductance (total inductance)                                      |
| iter                           |         |           |                |                                                                         |
| jt                             |         | A/m^2     | matrix         | toroidal current density (jt_dp + jt_di2)                               |
| jt_di2                         |         | A/m^2     | matrix         | toroidal current density due to plasma pressure                         |
| jt_dp                          |         | A/m^2     | matrix         | toroidal current density due to poloidal current                        |
| major_radius                   |         | m         | float          | major radius of plasma                                                  |
| minor_radius                   |         | m         | float          | minor radius of plasma                                                  |
| param_di2                      |         |           |                |                                                                         |
| param_dp                       |         |           |                |                                                                         |
| pol_current                    |         |           |                |                                                                         |
| pol_current_norm               |         |           |                |                                                                         |
| pressure                       |         | Pa        | matrix         | plasma pressure                                                         |
| pressure_norm                  |         | Pa        | array          | plasma pressure (p[0]: axis, p[-1]: surface)                            |
| pressure_vol_average           |         | Pa        | float          | volume-averaged plasma pressure                                         |
| pts                            |         |           |                |                                                                         |
|                                | r_rmin  | m         | float          | inside r                                                                |
|                                | z_rmin  | m         | float          | inside z                                                                |
|                                | r_rmax  | m         | float          | outside r                                                               |
|                                | z_rmax  | m         | float          | outside z                                                               |
|                                | r_zmin  | m         | float          | bottom r                                                                |
|                                | z_zmin  | m         | float          | bottom z                                                                |
|                                | r_zmax  | m         | float          | top r                                                                   |
|                                | z_zmax  | m         | float          | top z                                                                   |
| q_center                       |         |           | float          | safety factor at magnetic axis                                          |
| q_edge                         |         |           | float          | safety factor at plasma surface                                         |
| safety_factor                  |         |           | matrix         | safety factor                                                           |
| safety_factor_norm             |         |           | array          | safety factor (q[0]: axis, q[-1]: surface)                              |
| stored_energy                  |         | Joul      | float          | stored energy, W=(3/2)<p>V                                              |
| strike_points                  |         |           |                | strike points                                                           |
|                                | r<n>    | m         | float          |                                                                         |
|                                | z<n>    | m         | float          |                                                                         |
| toroidal_flux                  |         |           | array          | toroidal flux of plasma cross section, (tf[0]: axis, tf[last]: surface) |
| toroidal_flux_diff             |         |           |                |                                                                         |
| triangularity                  |         |           | float          | triangularity of plasma                                                 |
| vessel                         |         |           | matrix         | vacuum vessel (1:inside, 0:outside)                                     |
| volume                         |         | m^3       | float          | plamsa volume                                                           |