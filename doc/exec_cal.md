# Execution of calculation

## Setting of current directory

If you execute the equilibrium code in a subdirectory, the working directory must be the repository root directory.  
So put the following at the beginning of the file:  

```python
import os
os.chdir('c:\\your\\repository\\root\\directory')
```

Or, you can edit `.env` file located in the repository root with adding  
`ROOT_DIR = C:\\your\\repository\\root\\directory`.
Then,

```python
import os
os.chdir(os.getenv('ROOT_DIR'))
```

## Equilibrium calculation

Equilibrium calculation is executed with the following command.  
All the calculation results are assigned to the variable cond as a python dictionary type.

```shell
cond = sb.calc_equilibrium(condition)
```

### Negative pressure

If the calculation did not converge due to negative plasma pressure, please try to increase the vertical fields, or try to move plasma initial position r0 to outward.

### Calculated parameters

You can see what was calculated with '.keys()', which is a method to obtains a key list.

```python:
cond.keys()
```

[Explanation of input and output parameters](./1_params.md)