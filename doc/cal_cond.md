# Calculation condition

Before performing an equilibrium calculation, the calculation conditions must be set.The default calculation conditions are described in `/device/(device_name)/condition_default.py`.

## Setting of parameters

All parameters are in MKSA units unless otherwise noted.
For example, the parameter of 'ip':-100.0e+3 means -100.0 [kA] of plasma current.  

### TF coil

```python
    # TF current
    'cur_tf':{'tf': +50.0e+3, 'turn': 16, 
              #'rewind': True, # rewind of tf coil
              },
```

- cur_tf: setting of toroidal coil
  - tf : float  
    toroidal coil current
  - turn : int  
    number of toroidal coils
  - rewind: boolean, optional  
    presence or absence of rewind

### Plasma current

```python
    # initial plasma profile
    'cur_ip':{'ip':+100.0e+3, 'r0':0.75, 'z0':0.0, 'radius':0.3, 
              #'degree': 2.0
              },
```

- cur_ip : setting of initial plasma  
  - ip : float  
    plasma current
  - r0 : float  
    major radius of initial plasma  
  - z0 : float  
    vertical position of initial plasma
  - radius : float  
    minor radius of initial plasma
  - degree : float, default 2, optional  
    degree of initial plasma profile, ex. parabolic profile when degree = 2  
    ![flux](./images/fig_jt_profile.png)  

### PF coil

```python
    # PF currents
    'cur_pf':{'hcult16':0.0,'pf17t12':-1.0e+3, 'pf26t36':-1.5e+3,
              'pf4_1ab3_cc2':-3.0e+3,'pf35_2':1.5e+3, # this line enable: divertor, disable: limiter 
              },
```

- cur_pf : setting of pf coil currents
  
Available coil names are listed in `/device/(device_name)/coils/data_npy/`.  

### Constraints (Optional)

You can set the binding constraints for equilibrium calculations. Remove the comment out in the example to enable it.
The currently available binding constraints are as follows

- Plasma current
- Stored energy
- Plasma pressure
- Current density, jt
- Flux
- Br, br
- Bz, bz

```python
    # setting for constraints
    # 'constraints':{
    #     'arbitrary_name_ip':{'ip':100.0e+3, 'weight':1.0}, # plasma current [A]
   
    #     'arbitrary_name_se':{'stored_energy':100, 'weight':1.0}, # stored energy [Joul]
    
    #     'arbitrary_name_p0':{'point':(0.90, 0.0), 'pressure':0.7e+3, 'weight':1.0}, # pressure [Pa]
    #     'arbitrary_name_p1':{'point':(0.4, 0.0), 'pressure':30.0, 'weight':1.0}, # pressure [Pa] 
    #     'arbitrary_name_p2':{'point':'axis', 'pressure':0.7e+3, 'weight':1.0}, # axis is available.        

    #     'arbitrary_name_jt0':{'point':(0.3, 0.0), 'jt':20.0e+3, 'weight':1.0}, # plasma current density [A/m^2]
    #     'arbitrary_name_jt1':{'point':(0.4, 0.0), 'jt':30.0e+3, 'weight':1.0},  
    
    #     'arbitrary_name_fl0':{'point':(0.1985,  0.450), 'flux':0.003, 'weight':1.0}, # flux [Vsec or Wb]
    #     'arbitrary_name_fl1':{'point':(0.1985,  0.0  ), 'flux':0.007, 'weight':1.0},

    #     'arbitrary_name_br0':{'point':(0.2, 0.4), 'br': 0.01, 'weight':0.0}, # Br [T]
    #     'arbitrary_name_br1':{'point':(0.2, 0.0), 'br': 0.00, 'weight':0.0},  
    
    #     'arbitrary_name_bz0':{'point':(0.0, 0.4), 'bz':0.036, 'weight':0.0}, # Bz [T]
    #     'arbitrary_name_bz1':{'point':(0.0, 0.0), 'bz':0.060, 'weight':10.0},  
        
    #     },
```

Adjust the weight factor to 1, 10, 100, etc. to adjust the degree of constraint enforcement.  
The result of applying the constraint is added to the input condition.  
In the example below, when the pressure is set to 40Pa, the calculated result is 40.179Pa.  

```python:
cond['constraints']
```

```python:
{'name3': {'point': (0.6, 0.0),
  'pressure': 40.0,
  'weight': 1.0,
  'pressure_calc': 40.178728138256936}}
```

### Fix boundary (Optional)

Enable `fix_boundary` for fixed boundary equilibrium calculations.  
At this time, the current of the PF coil defined by `cur_pf` is regulated.
If `val` is not `lcfs`, a 95% poloidal flux is used.

```python
    # For fix boudary calculation. 
    # val: target value. (lcfs: last closed flux surface, other: 95% poloidal flus)
    # 'fix_boundary':{'arbitrary_name_b0':{'point':(0.28, 0.0),   'val':'lcfs', 'weight':1.0, }, 
    #                 'arbitrary_name_b1':{'point':(0.50, -0.76), 'val':'lcfs', 'weight':1.0, },
    #                 'arbitrary_name_b2':{'point':(1.20, 0.0),   'val':'lcfs', 'weight':1.0, },
    #                 'arbitrary_name_b3':{'point':(0.50, +0.76), 'val':'lcfs', 'weight':1.0, },
    #                 'arbitrary_name_b4':{'point':(0.88, +0.5),  'val':'lcfs', 'weight':1.0, },      
    #                 'arbitrary_name_b5':{'point':(0.88, -0.5),  'val':'lcfs', 'weight':1.0, },              
    #                 },     
```

### Specific location values (Optional)

For example, to find the value to be measured in a magnetic measurement, you can specify the location of the sensor and it will output the value at that location in the results.

```python
    # calculate flux (r, z): result is set to 'fl_val'.
    # 'fl_pos':{'arb_name_1':(0.1985, 0.450), },

    # calculate Br(r, z): result is set to 'br_val'
    #'br_pos':{'arb_name_1':(1.0, 1.0), },

    # calculate Bz(r, z): result is set to 'bz_val'
    #'bz_pos':{'arb_name_1':(0.0, 0.0), },
```

### Fixing of magnetic axis (Optional)

This flag may not be used very often.

```python
    # flag to fix magnetic axis at initial plasma profile (r0, z0)
    #'fix_pos': True,
```

### Degree of polynomial (Optional)

Default is 1.Enable when you want to increase the degree.

```python
    # number of polynomial coefficients
    # 'num_dpr':1, # dp/df
    # 'num_di2':1, # di2/df
```

### Calculation Termination Conditions (Optional)

The iterations of the equilibrium calculation count the number of times the value is worse than the previous one.When this number of times becomes more than the set value, the calculation is terminated.

```python
    #  maximum number of bad step: Calculation ends when exceeds 
    # 'bad_step_max':4,
```

### Convergence value setting

```python
    # Convergence judgment value
    # If the change of the current distribution (loss) falls below this value, 
    # the calculation is terminated as convergence.
    # 'conv_judge_val': 0.05, 
```

### Monitor values during iteration

```python
    # Parameters to monitor while performing equilibrium calculations (optional)
    # 'monitor_values':['axis_z', 'axis_r']    
```
