# Definition of inductances

- [Definition of inductances](#definition-of-inductances)
  - [Inductance](#inductance)
    - [Normalized internal inductance](#normalized-internal-inductance)
    - [Consider with flux](#consider-with-flux)

## Inductance

The total energy contained in the magnetic field produced by the loop.

```math
W=\int \frac{B^{2}}{2 \mu_{0}}dv=\int_{p} \frac{B^{2}}{2 \mu_{0}}dv + \int_{e} \frac{B^{2}}{2 \mu_{0}}dv
```

The integration area of the first term and the second terms are inside of plasma and outside of plasma, respectively.  
Note: $`B`$ is due to plasma, not including external coils.  
Thus,  

```math
\frac{1}{2}LI^{2} = \frac{1}{2}L_{i}I^{2} + \frac{1}{2}L_{e}I^{2}
```

Namely,

```math
L = L_{i}+L_{e}
```

$`L`$: self inductance  
$`L_{i}`$: internal inductance

```math
\frac{1}{2}L_{i}I^{2} = \int_{p} \frac{B_{\theta}^{2}}{2 \mu_{0}}dv = \frac{<B_{\theta}^{2}>_{v}V}{2 \mu_{0}}
```

$`B_{\theta}`$: poloidal magnetic field due to plasma, not including external coils.  
$`V`$: plasma volume  
$`<>_{v}`$: volume average

Thus,  

```math
L_{i} = \frac{<B_{\theta}^{2}>_{v}V}{\mu_{0} I^{2}}
```

### Normalized internal inductance

Consider the equation:

```math
\frac{1}{2}L_{0}I^{2} = \frac{B_{\theta}(a)^{2}V}{2 \mu_{0}}
```

Normalized internal inductance $`l_{i}`$ is defined as

```math
l_{i} = L_{i}/L_{0} = \frac{<B_{\theta}^{2}>_{v}}{B_{\theta}(a)^{2}}
```

It is often defined by the following formula, using cross-section average.

```math
l_{i} = \frac{<B_{\theta}^{2}>_{s}}{B_{\theta}(a)^{2}}
```

In this equilibrium code, the normalize internal inductance is calculated as follows.

$`2 \pi a B_{\theta}(a) = \mu_{0} I`$, $`\pi a^{2} = S`$  
$`S`$: Cross section of Plasma  

```math
B_{\theta}(a)^{2} = \frac{\mu_{0}^{2}I^{2}}{4 \pi S}
```

Thus,  

```math
l_{i}=\frac{4 \pi S <B_{\theta}^{2}>_{s}}{\mu_{0}^{2}I^{2}}
```

### Consider with flux

$`\Phi = \Phi_{i} + \Phi_{e}`$  
$`\Phi_{i}`$: poloidal flux of plasma inside  
$`\Phi_{e}`$: poloidal flux of plasma outside  

$`L I = L_{i}I+L_{e}I`$

Self inductance: $`L=\Phi/I`$  
Internal inductance: $`L_{i}=\Phi_{i}/I`$

See:  
<http://fusionwiki.ciemat.es/wiki/Internal_inductance>