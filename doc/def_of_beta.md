# Definitions of beta

- [Definitions of beta](#definitions-of-beta)
  - [poloidal beta](#poloidal-beta)
  - [toroidal beta](#toroidal-beta)
  - [normalized beta](#normalized-beta)

## poloidal beta

```math
\beta_{p}=\frac{<p>_{s}}{B_{\theta}^{2}(a) /2 \mu_{0}}
```

See, Tokamaks SECOND EDITION, JOHN WESSON, eq 3.5.1  

where,  

```math
2 \pi a B_{\theta}(a) = \mu_{0} I, \pi a^{2} = S
```

$`S`$: Cross section of Plasma  
and,  

```math
B_{\theta}(a) = \frac{\mu_{0}I}{2 \sqrt{\pi S}}
```

thus,  

```math
\beta_{p} = \frac{8 \pi <p>_{s} S}{\mu_{0} I^{2}}
```

## toroidal beta

```math
\beta_{t}=\frac{<p>_{v}}{B_{t0}^{2}/2 \mu_{0}}
```

$`B_{t0}`$: the vacuum toroidal magnetic ﬁeld at the magnetic axis  
$`<>_{v}`$: volume average

See: <http://www.jspf.or.jp/Journal/PDF_JSPF/jspf2003_02/jspf2003_02-121.pdf>

## normalized beta

```math
\beta_{t}[\%] = \beta_{N} \frac{I_{p}}{a B_{t}} [MA/m.T]
```

$`B_{t}`$: Toroidal magnetic field at the center of the plasma.  
Note that it is not a vacuum magnetic field  
$`a`$: minor radius

See: <https://www.jstage.jst.go.jp/article/jspf/79/2/79_2_123/_pdf>