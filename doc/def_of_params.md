# Definition of parameters

Contents:

- [Definition of parameters](#definition-of-parameters)
  - [Shape](#shape)
  - [Stored energy](#stored-energy)
  - [Decay index](#decay-index)
  - [Safety factor](#safety-factor)
  - [Magnetic shear and normalized pressure gradient](#magnetic-shear-and-normalized-pressure-gradient)
  - [Scaling law](#scaling-law)

## Shape

![Shape parameters](./images/quest_shape.drawio.svg)

## Stored energy

```math
W = 2 \times \frac{3}{2} \int p \: dv =3<p>_{v}V
```

Factor 2 means ion and electron.  
$`V`$: plasma volume  
$`<>_{v}`$: volume average  
See, Tokamaks SECOND EDITION, JOHN WESSON, eq 1.4.3

## Decay index

```math
n = -\frac{R}{B_{z}} \frac{\partial B_{z}}{\partial R}
```

## Safety factor

```math
q = \frac{d\phi}{d\psi}
```

$`\phi`$: toroidal flux , $`\psi`$: poloidal flux

The toroidal flux can be given by the following equation.

```math
\phi=\int B_{\phi}dS=\int dS\frac{\mu_{0}I}{2 \pi R }
```

```math
2 \pi R B_{\phi}=\mu_{0}I
```

Integration area is inside of flux surfaces. Thus, the $`\phi (x)`$ and its derivative function can be calculated numerically. And, the safety factor can be calculated as below.

```math
q = \frac{d\phi}{d\psi}=\frac{1}{\psi_{B}- \psi_{M}}\frac{d \phi (x)}{dx}
```

In other,

```math
\begin{align}
q = \frac{d\phi}{d\psi}
&=\frac{\mu_{0}}{2 \pi} \int dS \frac{1}{R} \frac{dI}{d\psi}\\
&=\frac{\mu_{0}}{4 \pi} \int dS \frac{1}{IR} \frac{dI^{2}}{d\psi}
\end{align}
```

Because,

```math
\frac{dI}{d\psi}=\frac{1}{2 I}\frac{dI^{2}}{d\psi}
```

$d\psi = (\psi_{B}- \psi_{M}) dx$ from $x = (\psi- \psi_{M}) / (\psi_{B}- \psi_{M})$

Finally,

```math
q =\frac{1}{\psi_{B}- \psi_{M}}\frac{\mu_{0}}{4 \pi} \int dS \frac{1}{IR} \frac{dI^{2}}{dx}
```

## Magnetic shear and normalized pressure gradient

```math
S = \frac{\partial q}{\partial \psi}, \quad \alpha = \frac{\partial p}{\partial \psi}
```

For large aspect ratio, it can be rewritten as:

```math
S=\frac{r}{q} \frac{\partial q}{\partial r}, \quad \alpha = \frac{2 \mu_{0}Rq^{2}}{B^{2}} \frac{\partial p}{\partial r}
```

ref: <https://www.jstage.jst.go.jp/article/ieejfms/124/5/124_5_393/_pdf>

## Scaling law

**ITER89-P**:  

```math
\tau_E^{ITER89-P} = 0.048 M^{0.5} I_p^{0.85} R^{1.2} a^{0.3} k^{0.5} n_{20}^{0.1} B^{0.2} P^{-0.5}
```

(in s, AMU, MA, m, m, elongation, $10^{20}$ $m^{-3}$, T, MW)

**IPB98(y, 2)**:  

```math
\tau^{ELMy}_{E, th} = 0.0562 I^{0.93} B^{0.15} P^{-0.69} n_{19}^{0.41} M^{0.19} R^{1.97} \epsilon^{0.58} \kappa_a^{0.78}
```

(in s, MA, T, MW, $10^{19}$ $m^{-3}$, AMU, m)

![IPB98](./images/table_IPB98.png)

**Glubus**:

```math
\tau^{Globus}_E = 0.066 I^{0.53} B^{1.05} P^{-0.58} n_{19}^{0.65} R^{2.66} \kappa^{0.78}
```

ref: <https://wiki.fusion.ciemat.es/wiki/Scaling_law>  
ITER89-P: [Nucl. Fusion 30 (1990) 1999](https://iopscience.iop.org/article/10.1088/0029-5515/30/10/001)  
IPB98(y, 1): [Nucl. Fusion 39 (1999) 2137](https://iopscience.iop.org/article/10.1088/0029-5515/39/12/301j)  
IPB98(y, 1-4): [Nucl. Fusion 48 (2008) 099801](https://iopscience.iop.org/article/10.1088/0029-5515/39/12/302)  
Globus: [Nucl. Fusion 62 (2022) 016011](https://iopscience.iop.org/article/10.1088/1741-4326/ac38c9)  
