# g-fileに関する考察

## 電流の表式

Grad-Shafranovの式は次で与えられる。

```math
j_t = 2 \pi R \frac{d P(\psi)}{d \psi} + \frac{\mu_0}{4 \pi R} \frac{d I^2(\psi)}{d \psi}
```

一方、g-fileの説明では、トロイダル電流の式が次で与えられている。

```math
j_t=R P'(\psi) + FF'(\psi)/R
```

ただし、$P'(\psi)$ , $FF'(\psi)$の次元がそれぞれ(nt/m^2)/(Weber/rad), (mT)^2/(Weber/rad)で与えられていることに注意する必要がある。
つまり、この式の微分は単位ラジアン$\psi_r (=\psi / 2 \pi)$当たりの微分である。

一方、ポロイダル電流と磁場の関係式より

```math
B=\frac{\mu_{0}}{2 \pi R} I
```

であって、g_fileの定義によれば、

```math
F = R B = \frac{\mu_0}{2 \pi} I
```

である。従ってポロイダル電流由来の項は、次のように変形できる。

```math
\frac{\mu_0}{4 \pi R} \frac{d I^2}{d \psi} = \frac{\pi}{\mu_0 R} \frac{d F^2}{d \psi} = \frac{2 \pi}{\mu_0 R} \frac{F d F}{d \psi} = \frac{1}{\mu_0 R} \frac{F d F}{d \psi_r}
```

従って、

```math
j_t = R \frac{d P}{d \psi_r} + \frac{1}{\mu_0 R}F \frac{dF}{d \psi_r}
```

```math
j_t = R P' + FF'/(\mu_0 R)
```

$\mu_0$の違いはあるものの、g-fileの式にほぼ同じになる。

## 実際のg-fileの場合

g-fileの説明では、
PRES: nt/m^2
PPRIME: (nt/m^2)/(Weber/rad)
という次元である。

```math
P'(\psi) = \frac{d P(\psi)}{d \psi_r}=2 \pi \frac{d P(\psi)}{d \psi}
```

上のような関係が成り立つが、一方、g-fileでは全て単位ラジアン当たりの値に統一されている。  
