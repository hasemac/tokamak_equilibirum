{
    # TF current
    'cur_tf':{'tf': +50.0e+3, 'turn': 16, 
              #'rewind': True, # rewind of tf coil
              },
    
    # initial plasma profile
    'cur_ip':{'ip':+100.0e+3, 'r0':0.75, 'z0':0.0, 'radius':0.3, 
              #'degree': 2.0
              },
    
    # PF currents
    'cur_pf':{'hcult16':0.0,'pf17t12':-1.0e+3, 'pf26t36':-1.5e+3,
              'pf4_1ab3_cc2':-3.0e+3,'pf35_2':1.5e+3, # this line enable: divertor, disable: limiter 
              },
    # setting for constraints
    # 'constraints':{
    #     'arbitrary_name_ip':{'ip':100.0e+3, 'weight':1.0}, # plasma current [A]
   
    #     'arbitrary_name_se':{'stored_energy':100, 'weight':1.0}, # stored energy [Joul]
    
    #     'arbitrary_name_p0':{'point':(0.90, 0.0), 'pressure':0.7e+3, 'weight':1.0}, # pressure [Pa]
    #     'arbitrary_name_p1':{'point':(0.4, 0.0), 'pressure':30.0, 'weight':1.0}, # pressure [Pa] 
    #     'arbitrary_name_p2':{'point':'axis', 'pressure':0.7e+3, 'weight':1.0}, # axis is available.        

    #     'arbitrary_name_jt0':{'point':(0.3, 0.0), 'jt':20.0e+3, 'weight':1.0}, # plasma current density [A/m^2]
    #     'arbitrary_name_jt1':{'point':(0.4, 0.0), 'jt':30.0e+3, 'weight':1.0},  
    
    #     'arbitrary_name_fl0':{'point':(0.1985,  0.450), 'flux':0.003, 'weight':1.0}, # flux [Vsec or Wb]
    #     'arbitrary_name_fl1':{'point':(0.1985,  0.0  ), 'flux':0.007, 'weight':1.0},

    #     'arbitrary_name_br0':{'point':(0.2, 0.4), 'br': 0.01, 'weight':0.0}, # Br [T]
    #     'arbitrary_name_br1':{'point':(0.2, 0.0), 'br': 0.00, 'weight':0.0},  
    
    #     'arbitrary_name_bz0':{'point':(0.0, 0.4), 'bz':0.036, 'weight':0.0}, # Bz [T]
    #     'arbitrary_name_bz1':{'point':(0.0, 0.0), 'bz':0.060, 'weight':10.0},  
        
    #     },
    
    # For fix boudary calculation. 
    # val: target value. (lcfs: last closed flux surface, other: 95% poloidal flus)
    # 'fix_boundary':{'arbitrary_name_b0':{'point':(0.28, 0.0),   'val':'lcfs', 'weight':1.0, }, 
    #                 'arbitrary_name_b1':{'point':(0.50, -0.76), 'val':'lcfs', 'weight':1.0, },
    #                 'arbitrary_name_b2':{'point':(1.20, 0.0),   'val':'lcfs', 'weight':1.0, },
    #                 'arbitrary_name_b3':{'point':(0.50, +0.76), 'val':'lcfs', 'weight':1.0, },
    #                 'arbitrary_name_b4':{'point':(0.88, +0.5),  'val':'lcfs', 'weight':1.0, },      
    #                 'arbitrary_name_b5':{'point':(0.88, -0.5),  'val':'lcfs', 'weight':1.0, },              
    #                 },        

    # calculate flux (r, z): result is set to 'fl_val'.
    # 'fl_pos':{'arb_name_1':(0.1985, 0.450), },

    # calculate Br(r, z): result is set to 'br_val'
    #'br_pos':{'arb_name_1':(1.0, 1.0), },

    # calculate Bz(r, z): result is set to 'bz_val'
    #'bz_pos':{'arb_name_1':(0.0, 0.0), },
    
    # flag to fix magnetic axis at initial plasma profile (r0, z0)
    #'fix_pos': True,
        
    # number of polynomial coefficients
    # 'num_dpr':1, # dp/df
    # 'num_di2':1, # di2/df
    
    #  maximum number of bad step: Calculation ends when exceeds 
    # 'bad_step_max':4,
    
    # Convergence judgment value
    # If the change of the current distribution (loss) falls below this value, 
    # the calculation is terminated as convergence.
    # 'conv_judge_val': 0.05, 
    
    # Parameters to monitor while performing equilibrium calculations (optional)
    # 'monitor_values':['axis_z', 'axis_r']    
}