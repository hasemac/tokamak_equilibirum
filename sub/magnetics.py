from cmath import inf
import numpy as np
from scipy.special import *
from scipy import constants as sc

#https://docs.scipy.org/doc/scipy/reference/constants.html#module-scipy.constants

# 全ての定数の表示
#sc.find()
# 特定の値の表示 (value, unit, uncertainty)
#sc.physical_constants["speed of light in vacuum"]

#u0 = 4*np.pi*1.0e-7 # [N/A**2]: 真空の透磁率
#e0 = 8.8541878128e-12 # : 真空の誘電率
#c0 = 1/(u0*e0)**(0.5) # [m/sec]: 光速
# 素電荷e: sc.e
# 電子の質量me: sc.m_e

u0 = sc.mu_0 # 真空の透磁率
e0 = sc.epsilon_0 # 真空の誘電率
c0 = sc.c # 光速
pi = sc.pi

def flux(r, z, rc, zc, ic):
    # r, z: 計算する位置
    # rc, zc: コイルの半径とz座標
    # ic: コイルの電流
    
    # 小数点10桁以下を無視
    r = np.round(r, 10)
    z = np.round(z, 10)
    rc = np.round(rc, 10)
    zc = np.round(zc, 10)
    
    if r <= 0.0 or rc <= 0.0:
        return 0.0
    if r == rc and z == zc:
        return 0.0
    k= 4*r*rc/((r+rc)**2+(z-zc)**2)
    fx = (2*pi*r)*(u0*ic/pi/k**0.5)*(rc/r)**0.5
    fx *= (1-k/2)*ellipk(k)-ellipe(k)
    if fx == inf:
        print(r, z, rc, zc, ic)
    return fx

def br(r, z, rc, zc, ic):
    # 小数点10桁以下を無視
    r = np.round(r, 10)
    z = np.round(z, 10)
    rc = np.round(rc, 10)
    zc = np.round(zc, 10)
        
    if r == rc and z == zc:
        return 0.0
    if r <= 0.0 or rc <= 0.0:
        return 0.0    
    k= 4*r*rc/((r+rc)**2+(z-zc)**2)
    d = (r-rc)**2+(z-zc)**2
    br = (u0*ic/4/pi)*(z-zc)/k**0.5/(r*rc)**0.5
    br *= 2*(2-k)*rc*ellipe(k)/d-k*ellipk(k)/r
    return br

def bz(r, z, rc, zc, ic):
    # 小数点10桁以下を無視
    r = np.round(r, 10)
    z = np.round(z, 10)
    rc = np.round(rc, 10)
    zc = np.round(zc, 10)
    
    if r == rc and z == zc:
        return 0.0
    # zero divisionを避ける
    if r <= 0.0:
        r = 10**(-7)
    if rc <= 0.0:
        rc = 10**(-7)
    
    k= 4*r*rc/((r+rc)**2+(z-zc)**2)
    d = (r-rc)**2+(z-zc)**2
    bz = (u0*ic/4/pi)*k**0.5/(r*rc)**0.5
    bz *= ellipk(k)-(r**2-rc**2+(z-zc)**2)*ellipe(k)/d
    return bz

def conv_cart_to_cylind(cal_point, mag):
    """conv mag in cartesian to mag in cylindrical

    Args:
        cal_point (tuple): position to calculate in cartesian
        mag (tuple): mag in cartesian (bx, by, bz)

    Returns:
        tuple: mag in cylindrical (bt, br, bz)
    """
    x, y, z = cal_point
    bx, by, bz = mag
    r = np.sqrt(x**2 + y**2)
    cos = x/r
    sin = y/r
    br = bx * cos + by * sin
    bt = -bx * sin + by * cos
    return (bt, br, bz)

def calmag_line_coil(cal_point, current ,line_coil, cylindrical=True):
    """_summary_

    Args:
        cal_point (tuple): position to calculate in cartesian
        current (float): current
        line_coil (array of lines): [lines0, lines1,,,,]

    Returns:
        tuple: magnetic field (bt, br, bz) if cylindral = True (default)
        tuple: magnetic field (bx, by, bz) if cylindral = False
    """
    res = np.zeros(3)
    for lines in line_coil:
        res += np.array(calmag_connected_lines(cal_point, current, lines))
    
    res = tuple(res)
    
    if not cylindrical:
        return res
    
    return conv_cart_to_cylind(cal_point, res)

def calmag_connected_lines(cal_point, current, lines):
    # 連結された直線が作り出す磁場の計算
    # cal_point: (x0, y0, z0): 計算する位置
    # current: コイル電流
    # lines: 線を表す点の配列
    """Calculating the magnetic field created by connected straight lines

    Args:
        cal_point (tuple): position to calculate
        current (float): current
        lines (dict): lines['points'] (array of points), lines['turn'] (int)
        lines['points'] = [(x0, y0, z0), (x1, y1, z1),,,,]
        
    Returns:
        tuple: magnetic field (bx, by, bz)
    """
    turn = lines['turn']
    points = lines['points']
    
    res = np.zeros(3)
    for i in range(1, len(points)):
        p_start = points[i-1]
        p_end   = points[i]
        res += np.array(calmag_one_line(cal_point, current, p_start, p_end))
        
    res *= turn
    
    return tuple(res)
    
def calmag_one_line(cal_point, current, start_point, end_point):
    # 有限の長さの一つの直線が作る磁場の計算
    # cal_point: (x0, y0, z0): 計算する位置
    # start_point: (x1, y1, z1): 直線の開始点
    # end_point: (x2, y2, z2): 直線の終了点
    """Magnetic field created by a single finite length linear current

    Args:
        cal_point (tuple): _description_
        current (float): _description_
        start_point (tuple): _description_
        end_point (tuple): _description_

    Returns:
        tuple: magnetic field (bx, by, bz)
    """
    x0, y0, z0 = cal_point
    x1, y1, z1 = start_point
    x2, y2, z2 = end_point

    coef0 = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1)
    coef1 = 2*(x0-x1)*(x1-x2) + 2*(y0-y1)*(y1-y2) + 2*(z0-z1)*(z1-z2)
    coef2 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2)

    nume1 = y0*(z1-z2) + y1*(z2-z0) + y2*(z0-z1)
    nume2 = z0*(x1-x2) + z1*(x2-x0) + z2*(x0-x1)
    nume3 = x0*(y1-y2) + x1*(y2-y0) + x2*(y0-y1)

    ele = 2*coef1/np.sqrt(coef0)/(coef1*coef1-4*coef0*coef2)-2*(coef1+2*coef2)/np.sqrt(coef0+coef1+coef2)/(coef1*coef1-4*coef0*coef2)
    ele = 10.0**(-7)*current*ele
				
    xm = nume1*ele
    ym = nume2*ele
    zm = nume3*ele

    return (xm, ym ,zm)
    
    

        