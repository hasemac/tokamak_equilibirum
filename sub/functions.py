import numpy as np
import copy
import plasma.pmat as pmat
import plasma.pmat_br as pmat_br
import plasma.pmat_bz as pmat_bz
import vessel.vmat as vmat
import sub.emat as emat
import coils.cmat as cmat
from global_variables import gparam
from scipy.interpolate import interp1d
from scipy import constants as sc
import sub.plot as pl
import sub.sub_func as ssf
import sub.magnetics_cond as smc

gl = gparam()

# 正規化フラックスの計算
def get_normalized_flux(cond):
    dm_flux = cond["flux"]
    dm_domain = cond["domain"]
    # dm_flux: total flux (coil + plasma)
    # dm_domain: domain
    # return: dmat, normalized flux
    # 0: 磁気軸、1: 最外殻磁気面、0: 磁気面外
    res = emat.get_dmat_dim(dm_domain)
    faxis, fsurf = cond["f_axis"], cond["f_surf"]
    d = dm_domain["matrix"]
    f = dm_flux["matrix"]
    f = (f - faxis) / (fsurf - faxis)  # normalized flux
    f *= d  # domainの外にあるのはゼロにする。
    # 正規化フラックスは0(axis)-1(boundary)の間の数値
    # 0(axis)のf_axisは２次関数近似、近似がずれて、たまに負の値を出す。
    # というように必ず、0-1の範囲に収めるとする。
    f[f < 0] = 0.0 
    f[f > 1] = 1.0
    #print(f'fmax:{np.max(f)}, fmin:{np.min(f)}')
    res["matrix"] = f
    return res

def get_arr_diff(params, arr_norm_flux):
    """正規化フラックスからdp, またはdi2を算出

    Args:
        params (list[float]): 多項式の係数
        arr_norm_flux (array[float]): 正規化フラックスの1次元配列

    Returns:
        val (array[float]): 要素数は正規化フラックスの要素数に同じ
    """
    f = arr_norm_flux
    num = len(params)
    p0 = np.array([(f**i - f**num) for i in range(num)])
    p0 = p0.transpose()
    p0 = np.dot(p0, params)
    return p0

def get_arr(params, arr_norm_flux, cond):
    f = arr_norm_flux
    num = len(params)

    # 多項式の各項, n, p
    # an: x^n-x^p, これを積分すると次の式
    # an: x^(n+1)/(n+1)-x^(p+1)/(p+1), 1/(n+1)-1/(p+1) if x = 1
    # 例えば最外殻磁気面(x=1)ではプラズマ圧力がゼロとするならオフセットを各項に足す必要ある。つまり、
    # an: (x^(n+1)-1)/(n+1)-(x^(p+1)-1)/(p+1)

    # 積分した各項の行列
    p1 = np.array(
        [
            ((f ** (i + 1) - 1) / (i + 1) - (f ** (num + 1) - 1) / (num + 1))
            for i in range(num)
        ]
    )
    p1 = p1.transpose()
    p1 = np.dot(p1, params)
    
    # 積分の変数変換に伴う係数をかける。
    p1 *= (cond['f_surf']-cond['f_axis'])

    return p1        

def set_ecr_position(cond):
    if not 'ecr' in cond.keys():
        return

    i = cond['cur_tf']['tf'] * cond['cur_tf']['turn']    

    for key in cond['ecr'].keys():    
        freq = cond['ecr'][key]['freq']
    
        # freq=eB/(2 pi me)
        # 2 pi R B = mu0 i
        # Thus, R = mu0 e i /(4 pi^2 f me)
        r = sc.mu_0 * sc.elementary_charge * i / (4.0*sc.pi**2*freq*sc.electron_mass)
        cond['ecr'][key]['pos'] = r * np.array(cond['ecr'][key]['ordinal'])

# 規格化フラックスでの圧力の微分dP/dxと、圧力Pの計算
def get_dpress_press_normal(cond, num):
    params = cond['param_dp']
    f = np.linspace(0.0, 1.0, num)
    
    # 圧力の微分に関する行列
    p0 = get_arr_diff(params, f)
    
    # 圧力に関する行列
    p1 = get_arr(params, f, cond)
        
    return p0, p1

# 圧力の微分dP/dxと、圧力Pの計算
def get_dpress_press(cond):
    dm_normalizedflux = cond["flux_normalized"]
    dm_domain = cond["domain"]
    params = cond["param_dp"]    
    
    g = emat.dm_array(dm_domain)
    nr, nz = g.nr, g.nz

    f = dm_normalizedflux["matrix"].reshape(-1)
    d = dm_domain["matrix"].reshape(-1)

    # 最外殻磁気面の内部のみ取り出す
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    f = f[d == 1]

    # 圧力の微分に関する行列
    p0 = get_arr_diff(params, f)
    
    m_dp = np.zeros((nz, nr))
    for v, i, j in zip(p0, ir, iz):
        m_dp[j, i] = v

    # 圧力に関する行列
    p1 = get_arr(params, f, cond)
        
    m_pr = np.zeros((nz, nr))
    for v, i, j in zip(p1, ir, iz):
        m_pr[j, i] = v

    # 圧力に関してはx=1でp=0になるようにしてある。
    dm_dp = emat.get_dmat_dim(dm_domain)
    dm_pr = emat.get_dmat_dim(dm_domain)
        
    dm_dp["matrix"] = m_dp
    dm_pr["matrix"] = m_pr

    return dm_dp, dm_pr

def get_di2total_itotal(cond, arr_norm_flux):

    f = arr_norm_flux
    
    # この時点で下のように調節することも考えられるが、
    # 調節した点で、やはり計算誤差が出やすい。
    # 従って、不定形di2/iを計算するときにのみ
    # 補正を入れたほうが良い
    #f = [e if e != 1.0 else 0.999 for e in arr_norm_flux]
    #f = np.array(f)
        
    params = cond['param_di2']
    
    # TFcoilによるポロイダル電流
    i0 = cond["cur_tf"]["tf"]*cond["cur_tf"]["turn"]
    
    # I^2の微分に関する行列
    di2 = get_arr_diff(params, f)
    
    # I^2に関する行列, トロイダルコイルによる成分も加算
    i2 = get_arr(params, f, cond) + i0**2
    
    # ここで正負の判定をする。
    #print('i2:', i2)
    # 場合によっては負の数を返すこともあるみたいなので、
    # その場所はゼロにする。
    i2[i2 < 0] = 0.0
    
    # I^2なのでIにする。
    i = np.sqrt(i2)

    # iは正負の任意性がある。
    # トロイダルコイル電流が負の場合は、x=1(最外殻磁気面）で
    # マイナスになるので、値を反転する。
    if i0 < 0:
        i *= -1.0
    
    return di2, i

# 規格化フラックス内でのポロイダルカレントの微分とポロイダルカレントの計算
def get_di2_i_norm(cond, num):
    """規格化フラックスでのdi2, iを返す。
    np.linspec(0.0, 1.0, num)の正規化フラックス
    x = 0.0 (axis) - 1.0 (boundary)

    Args:
        cond (dict): 平衡計算結果
        num (int): 返す個数

    Returns:
        array_float, array_float: dI^2/df_norm, I
    """

    f = np.linspace(0.0, 1.0, num)
    
    di2total, itotal = get_di2total_itotal(cond, f)

    return di2total, itotal
             
# ポロイダルカレントの微分dI^2/dxとポロイダルカレントの計算
def get_di2_i(cond):
    """di^2/dfとiのdmatを返す。

    Args:
        cond (dict): calculation result

    Returns:
        tuple of dmat: di2, i
    """
    dm_normalizedflux = cond["flux_normalized"]
    dm_domain = cond["domain"]
        
    g = emat.dm_array(dm_domain)
    nr, nz = g.nr, g.nz

    f = dm_normalizedflux["matrix"].reshape(-1)
    d = dm_domain["matrix"].reshape(-1)

    # 最外殻磁気面の内部のみ取り出す
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    f = f[d == 1]

    di2total, itotal = get_di2total_itotal(cond, f)
    
    m_di2 = np.zeros((nz, nr))
    for v, i, j in zip(di2total, ir, iz):
        m_di2[j, i] = v
        
    m_i = np.zeros((nz, nr))
    for v, i, j in zip(itotal, ir, iz):
        m_i[j, i] = v
        
    # プラズマの存在しない領域のポロイダル電流を設定
    # これはTFコイル電流に依存する。
    m_i[m_i == 0] = cond["cur_tf"]["tf"]*cond["cur_tf"]["turn"]
    
    dm_di2 = emat.get_dmat_dim(dm_domain)
    dm_i = emat.get_dmat_dim(dm_domain)
     
    dm_di2["matrix"] = m_di2
    dm_i["matrix"] = m_i

    return dm_di2, dm_i

# flux値の極小位置の探索
def search_local_min(dm_flx, dm_vv):
    g = emat.dm_array(dm_flx)
    nr, nz = g.nr, g.nz

    fl, vv = dm_flx["matrix"], dm_vv["matrix"]

    # 真空容器の中心位置から探索を開始
    r, z = gl.vessel_center
    ir, iz = gl.get_coarse_grid_num(r, z)
    
    # while vv[iz, ir] == 1:  # 真空容器外になったら探索終了
    #     if fl[iz, ir] > fl[iz + 1, ir]:
    #         iz += 1
    #     elif fl[iz, ir] > fl[iz - 1, ir]:
    #         iz -= 1
    #     elif fl[iz, ir] > fl[iz, ir + 1]:
    #         ir += 1
    #     elif fl[iz, ir] > fl[iz, ir - 1]:
    #         ir -= 1
    #     else:
    #         break
    
    dz = [0, 1, 1, 0, -1, -1, -1, 0, 1] 
    dr = [0, 0, 1, 1, 1, 0, -1, -1, -1]
    while vv[iz, ir] == 1:  # 真空容器外になったら探索終了
        val = [
            fl[iz    , ir    ], # 0: center
            fl[iz + 1, ir    ], # 1: up
            fl[iz + 1, ir + 1], # 2: up-right
            fl[iz    , ir + 1], # 3: right
            fl[iz - 1, ir + 1], # 4: down-right
            fl[iz - 1, ir    ], # 5: down
            fl[iz - 1, ir - 1], # 6: down-left
            fl[iz    , ir - 1], # 7: left
            fl[iz + 1, ir - 1], # 8: up-left
        ]
        k = np.argmin(val) # 周囲の最小値の場所
        if 0 == k:
            break
        iz += dz[k]
        ir += dr[k]
        # print(f'ir: {ir}, iz: {iz}')

    if vv[iz, ir] == 0:
        ir, iz = 0, 0
        
    #print(f'ir: {ir}, iz: {iz}')
    return (ir, iz)

# 磁気面をR方向に探索
def search_surface_r(cond, name='domain', f_surf_val=None):
    """Search the magnetic surface in the R direction

    Args:
        cond (dict): 計算の履歴
        name (str, optional): 探索する上でのマスクの指定. Defaults to 'domain'.
            'domain'または'xshape' マスクが1になっている個所で探索する。
        value (_type_, optional): 探索するポロイダルフラックスの値. Defaults to None.
            Noneの場合、最外殻磁気面のポロイダルフラックスになる。
            
        ex. search_surface_r(cond) : 最外殻磁気面の探索
            search_surface_r(cond, name='xshape') : 最外殻磁気面とダイバータレッグの探索
            search_surface_r(cond, value= (float val.)) : float_valのポロイダルフラックスの磁気面

    Returns:
        list of (r, z): 磁気面の座標
    """
    ef = emat.dm_array(cond['flux'])
    flx = cond['flux']['matrix']
    fsu = cond['f_surf']
    if f_surf_val is not None:
        fsu = f_surf_val

    # 左にシフトしたものを掛け合わせて、負になればその場所で最外殻磁気面の値
    ff = (flx-fsu)*(ssf.shift_x(flx, -1, 0.0)-fsu)
    sf = ff.reshape(-1)
    
    dom = cond['domain']['matrix']
    if name == 'xshape':
        dom = cond[name]['matrix']
    dmr = dom.reshape(-1)
    dml = ssf.shift_x(dom, -1, 0.0).reshape(-1) # 左にシフト    

    ir = ef.ir[sf < 0] # 負になっているir
    iz = ef.iz[sf < 0] # 負になっているiz
    # 注目している場所がドメインに接しているか確認するための変数。
    # 正であれば、どちらかのメッシュはドメイン内にあるという事。
    dmc = dmr[sf < 0] + dml[sf < 0]
    # domainに接しているもののみを取り出す。
    ir = ir[dmc > 0.0]
    iz = iz[dmc > 0.0]
    
    r1 = ef.rmin + ef.dr * ir
    r2 = ef.rmin + ef.dr * (ir+1)
    fr1 = flx[iz, ir]
    fr2 = flx[iz, ir+1]
    # flxの値に応じてrを計算
    r0 = r1 + (fsu-fr1)/(fr2-fr1) * (r2-r1)
    z0 = ef.zmin + iz * ef.dz
    
    return [(r, z) for r,z in zip(r0, z0)]

# 殻磁気面をz方向に探索
def search_surface_z(cond, name='domain', f_surf_val=None):
    ef = emat.dm_array(cond['flux'])
    flx = cond['flux']['matrix']
    fsu = cond['f_surf']    
    if f_surf_val is not None:
        fsu = f_surf_val
        
    # 上にシフトしたものを掛け合わせて、負になればその場所で最外殻磁気面の値
    ff = (flx-fsu)*(ssf.shift_y(flx, -1, 0.0)-fsu)
    sf = ff.reshape(-1)
    
    dom = cond['domain']['matrix']
    if name == 'xshape':
        dom = cond[name]['matrix']
    dmr = dom.reshape(-1)
    dml = ssf.shift_y(dom, -1, 0.0).reshape(-1) # 上にシフト    

    ir = ef.ir[sf < 0] # 負になっているir
    iz = ef.iz[sf < 0] # 負になっているiz
    # 注目している場所がドメインに接しているか確認するための変数。
    # 正であれば、どちらかのメッシュはドメイン内にあるという事。
    dmc = dmr[sf < 0] + dml[sf < 0]
    # domainに接しているもののみを取り出す。
    ir = ir[dmc > 0.0]
    iz = iz[dmc > 0.0]
    
    z1 = ef.zmin + ef.dz * iz
    z2 = ef.zmin + ef.dz * (iz+1)
    fz1 = flx[iz, ir]
    fz2 = flx[iz+1, ir]
    # flxの値に応じてrを計算
    r0 = ef.rmin + ir * ef.dr
    z0 = z1 + (fsu-fz1)/(fz2-fz1) * (z2-z1)
    
    return [(r, z) for r,z in zip(r0, z0)]

# 点を場所の近い順に並べなおす
def sort_points_short_distance(pts):
    p0 = pts.pop(0)
    sortpts = [p0]
    while len(pts) > 0:
        distsq = [(p0[0]-r)**2 + (p0[1]-z)**2 for r, z in pts]
        i = np.argmin(np.array(distsq))
        p0 = pts.pop(i)
        sortpts.append(p0)
    return sortpts

# 両隣の距離の近い点を間引く
def thin_out_points(pts, num = 50):
    # 最初に近い順に並びなおす。
    sortpts = sort_points_short_distance(pts)

    sortpts2 = np.roll(sortpts, -1, axis=0) # 点の配列を左にシフト
    # 右隣の点との距離
    dist0 = [np.sqrt((p0[0]-p1[0])**2 + (p0[1]-p1[1])**2) 
            for p0, p1 in zip(sortpts, sortpts2)]    
    # 左隣の点との距離
    dist1 = np.roll(dist0, 1)
    # 両隣の点との距離の和
    dist = list(dist0 + dist1)

    # 距離の和が小さいものは間引くことが可能
    while len(sortpts) > num:
        #print(len(sortpts), len(points))
        if len(sortpts) == 0:
            # 間引く点がなくなった場合は終了
            break
        i = np.argmin(dist)

        # 両隣の点を取得
        r0, z0 = sortpts[i-1]
        r1, z1 = sortpts[i]
        r2, z2 = sortpts[i+1] if i+1 < len(sortpts) else sortpts[0]
        
        # l0: 元々の両隣の点の距離
        # l1: 現在の両隣の点の距離        
        l0 = dist.pop(i)
        l1 = np.sqrt((r0-r1)**2+(z0-z1)**2)
        l1 += np.sqrt((r1-r2)**2+(z1-z2)**2)
        if l0 != l1:
            # 既に隣の点が間引かれていた場合は、そこで終了。
            break        
        sortpts.pop(i) # その点を間引く

    # 最後、もう一度並びなおす。

    return sort_points_short_distance(sortpts)

def find_lcfs(cond, num = 50):
    ptsr = search_surface_r(cond)
    ptsz = search_surface_z(cond)
    pts = ptsr + ptsz
    
    while len(pts) > num:
        pts = thin_out_points(pts, num=num)
    return pts

def find_x_boundary(cond, num=200):
    ptsr = search_surface_r(cond, name='xshape')
    ptsz = search_surface_z(cond, name='xshape')
    pts = ptsr + ptsz
    
    while len(pts) > num:
        pts = thin_out_points(pts, num=num)    
    return pts

def find_mag_surf_95(cond, num =80):
    val = (cond['f_surf']-cond['f_axis'])*0.95 + cond['f_axis']
    ptsr = search_surface_r(cond, f_surf_val=val)
    ptsz = search_surface_z(cond, f_surf_val=val)
    pts = ptsr + ptsz
    
    while len(pts) > num:
        pts = thin_out_points(pts, num=num)
    return pts
    
# 最外殻磁気面の探索(ip正負両方に対応)
def search_domain(cond):
    res = search_dom(cond)
    if None != res:    
        return res

    # 極小値の探索に失敗しているのでfluxを反転させて再探索
    dm_flx = cond["flux"]
    dm_flx["matrix"] = -dm_flx["matrix"]
    # a = dm_flx['matrix']
    # dm_flx2 = get_dmat_dim(dm_flx)
    # dm_flx2['matrix'] = -a

    res = search_dom(cond)
    
    # 反転させたものを元に戻しておく。
    dm_flx["matrix"] = -dm_flx["matrix"]
    
    if None == res:
        # 反転させても探索失敗の場合
        cond['cal_result'] = -1
        cond['error_messages'] += 'Can not find domain.'
        return res
    
    # これも反転させておく。
    cond["f_axis"]    = -cond["f_axis"]
    cond["f_surf"]    = -cond["f_surf"]
    cond["f_surf_95"] = -cond["f_surf_95"]
    return res

# 最外殻磁気面の探索(最小値)
def search_dom(cond):
    # dm_flx: fluxのdmat
    # dm_vv: 真空容器のdmat
    dm_flx = cond["flux"]
    dm_vv = cond["vessel"]
    g = emat.dm_array(dm_flx)
    nz, nr = g.nz, g.nr
    dz, dr = g.dz, g.dr
    zmin, rmin = g.zmin, g.rmin

    # 真空容器内の極小値を探索
    k, l = search_local_min(dm_flx, dm_vv)
    if 0 == k and 0 == l:
        return None

    # 値を記録
    cond["axis_ir"] = k
    cond["axis_iz"] = l
    # 近傍9点のデータから極値の補正値を求める。
    zc, rc, fax = ssf.find_extremum_loc_and_val(dm_flx['matrix'][l-1:l+2, k-1:k+2]) # 補正
    cond["axis_r"] = rmin + k * dr + rc * dr
    cond["axis_z"] = zmin + l * dz + zc * dz
    cond['f_axis'] = fax

    # 磁気軸のフラックスを保存し、
    # 最外殻磁気面とヌル点フラックスの初期値を設定
    #fax = dm_flx["matrix"][l, k]
    fsurf = fax

    # 一次元配列を作成
    m0 = dm_flx["matrix"].reshape(-1)
    vv = dm_vv["matrix"].reshape(-1)
    ir = g.ir
    iz = g.iz

    # 小さい値順に並び替える
    ix = m0.argsort()
    m0, ir, iz, vv = m0[ix], ir[ix], iz[ix], vv[ix]

    # プラズマ領域の初期化
    dm = np.zeros((nz, nr))
    dm[l, k] = 1.0  # 探索用のシードを極小値を持つ場所にセット
    dm2 = np.pad(dm, [(1, 1), (1, 1)]) # パディング
    
    # プライベート領域の初期化
    dm = np.zeros((nz, nr))
    dmpr = np.pad(dm, [(1, 1), (1, 1)]) # パディング

    # 探索の方法
    # 基本的には値の低いところに少しずつ水をためていき、
    # その水が真空容器に触れたらリミター、隣の水に触れたらダイバータという判定方法
    #    
    # 磁束の小さい値から順に探索。
    # 新しい点が、プラズマに接していればその点をプラズマの領域に追加、
    # 接していなければ、その点をプライベート領域に追加
    # 新しい点が真空容器に接したらリミター配位として
    # 新しい点がプライベート領域に接したらダイバータ配位として探索終了
    cond['conf_div'] = -1 # 最初は配位が定まらないとして-1をセット
    for f, i, j, v in zip(m0, ir, iz, vv):
        ni, nj = i + 1, j + 1  # paddingしているので、1を足す。
        
        # 注目点周辺(プラズマ領域)
        a = dm2[nj - 1 : nj + 2, ni - 1 : ni + 2].reshape(-1)
        a = np.sum(a)  # 近接点にプラズマが存在していること。
        # 新しい点に対する判定
        con = a > 0  # 存在していれば1、その他はゼロ
        
        # 注目点周辺(プライベート領域)
        a = dmpr[nj - 1 : nj + 2, ni - 1 : ni + 2].reshape(-1)
        a = np.sum(a)
        con_pr = a > 0 # プライベート領域に接していれば1、その他はゼロ
        
        if con and 1 == v:
            # 他のプラズマと接触して、かつ真空容器内
            # この場合、プラズマの存在領域として登録
            dm2[nj, ni] = 1
            fsurf = f
        
        elif not con and 1 == v:
            # 接触しておらず、真空容器内
            # この場合はダイバータ配位のプライベートリージョン
            # ややこしいのは、プラズマと接しないプライベートもあるということ
            # ヌル点のフラックス値を記録しておく。
            dmpr[nj, ni] = 1    
        
        elif not con and 0 == v:
            # 接触しておらず、真空容器外
            # この場合はなにもしない。
            pass
        
        # 探索の終了条件
        if con and 0 == v:
            # 他のプラズマと接触して且つ真空容器外ならリミター配位として探索終了
            cond["conf_div"] = 0
            break
        elif con and con_pr and 1 ==v:
            # プラズマに接触し、プライベート領域にも接したらダイバータ配位として探索終了
            cond["conf_div"] = 1
            cond["xpoint_ir"] = i
            cond["xpoint_iz"] = j
            break

        #print(f'fax: {fax}, fsurf: {fsurf}, fnull: {fnull}')

    # パディング分を取り除く
    dm3 = dm2[1 : 1 + nz, 1 : 1 + nr]
        
    dm3[iz[m0 >= fsurf], ir[m0 >= fsurf]] = 0
    #print(f'fs:{fsurf}')
    
    # 返り値の作成
    res = emat.get_dmat_dim(dm_flx)
    res["matrix"] = dm3
    
    cond["f_surf"] = fsurf
    
    # 95%ポロイダルフラックス値を計算
    cond['f_surf_95'] = (cond['f_surf']-cond['f_axis'])*0.95 + cond['f_axis']
    
    # 探索したdomainが4メッシュしかないときはエラー
    if np.sum(dm3) <= 4:
        cond['cal_result'] = -1
        cond['error_messages'] += 'The domain is now too small.'
        
    return res

# 体積平均の算出
def get_volume_average(dm_val, dm_domain):
    # dm_val: 例えばプラズマ圧力など
    g = emat.dm_array(dm_domain)
    dr, dz = g.dr, g.dz

    m = dm_domain["matrix"].reshape(-1)
    v = dm_val["matrix"].reshape(-1)

    # domain内のみ考える。
    r = g.r[m == 1]
    v = v[m == 1]

    vol = np.sum(2 * np.pi * r * dr * dz)  # plasma volume
    v = np.sum(2 * np.pi * r * dr * dz * v)  # vol*val
    return v / vol

# ポロイダル断面平均の算出
def get_cross_section_average(dm_val, dm_domain):
    g = emat.dm_array(dm_domain)
    dr, dz = g.dr, g.dz

    m = dm_domain["matrix"].reshape(-1)
    v = dm_val["matrix"].reshape(-1)

    # domain内のみ考える。
    v = v[m == 1]
    
    csec = np.sum(m[m == 1]) * dr * dz # cross section
    val = np.sum(dr * dz * v)
    
    return val/csec

# 最外殻磁気面形状
def set_domain_params(cond):
    d_mat = cond["domain"]
    g = emat.dm_array(d_mat)
    rmin, dr = g.rmin, g.dr
    zmin, dz = g.zmin, g.dz

    m = d_mat["matrix"].reshape(-1)
    ir = g.ir[m == 1]
    iz = g.iz[m == 1]
    r = g.r[m == 1]

    if 0 == len(ir) or 0 == len(iz) or 0 == len(r):
        cond["cal_result"] = -1
        cond["error_messages"] += 'Can not define domain_params.'
        return
        
    fl = cond['flux']['matrix']
    fsur = cond['f_surf']
    
    # ptsの辞書型での初期化
    cond["pts"] = {}
    
    # rminがある位置
    v = np.min(ir)
    #r_rmin = rmin + dr * np.mean(ir[ir == v])
    #z_rmin = zmin + dz * np.mean(iz[ir == v])
    kr = int(np.mean(ir[ir == v]))
    kz = int(np.mean(iz[ir == v]))
    p = ssf.find_points_of_quad_func(fl[kz-1:kz+2, kr-1:kr+2], fsur)
    i = np.argmin([r for z, r in p])
    cz, cr = p[i]
    r_rmin = rmin + dr * (kr+cr)
    z_rmin = zmin + dz * (kz+cz)
    cond["pts"]["r_rmin"] = r_rmin
    cond["pts"]["z_rmin"] = z_rmin

    
    # rmaxがある位置
    v = np.max(ir)
    #r_rmax = rmin + dr * np.mean(ir[ir == v])
    #z_rmax = zmin + dz * np.mean(iz[ir == v])
    kr = int(np.mean(ir[ir == v]))
    kz = int(np.mean(iz[ir == v]))
    p = ssf.find_points_of_quad_func(fl[kz-1:kz+2, kr-1:kr+2], fsur)
    i = np.argmax([r for z, r in p])
    cz, cr = p[i]
    r_rmax = rmin + dr * (kr+cr)
    z_rmax = zmin + dz * (kz+cz)
    cond["pts"]["r_rmax"] = r_rmax
    cond["pts"]["z_rmax"] = z_rmax

    # zminがある位置
    v = np.min(iz)
    #r_zmin = rmin + dr * np.mean(ir[iz == v])
    #z_zmin = zmin + dz * np.mean(iz[iz == v])
    kr = int(np.mean(ir[iz == v]))
    kz = int(np.mean(iz[iz == v]))
    p = ssf.find_points_of_quad_func(fl[kz-1:kz+2, kr-1:kr+2], fsur)
    i = np.argmin([z for z, r in p])
    cz, cr = p[i]
    r_zmin = rmin + dr * (kr+cr)
    z_zmin = zmin + dz * (kz+cz)
    cond["pts"]["r_zmin"] = r_zmin
    cond["pts"]["z_zmin"] = z_zmin

    # zmaxがある位置
    v = np.max(iz)
    #r_zmax = rmin + dr * np.mean(ir[iz == v])
    #z_zmax = zmin + dz * np.mean(iz[iz == v])
    kr = int(np.mean(ir[iz == v]))
    kz = int(np.mean(iz[iz == v]))
    p = ssf.find_points_of_quad_func(fl[kz-1:kz+2, kr-1:kr+2], fsur)
    i = np.argmax([z for z, r in p])
    cz, cr = p[i]
    r_zmax = rmin + dr * (kr+cr)
    z_zmax = zmin + dz * (kz+cz)
    cond["pts"]["r_zmax"] = r_zmax
    cond["pts"]["z_zmax"] = z_zmax

    a0 = (r_rmax - r_rmin) / 2.0
    r0 = (r_rmax + r_rmin) / 2.0
    cond["major_radius"] = r0
    cond["minor_radius"] = a0
    cond["aspect_ratio"] = r0/a0
    cond["elongation"] = (z_zmax - z_zmin) / (r_rmax - r_rmin)
    cond["triangularity"] = (r0 - r_zmax) / a0
    cond["volume"] = np.sum(2 * np.pi * r * dr * dz)
    cond["cross_section"] = np.sum(m[m == 1]) * dr * dz

def set_domain_params_95(cond):
    pts = cond['boundary_flux95']
    r, z = [e[0] for e in pts], [e[1] for e in pts]
    
    i = np.argmax(r)
    r_rmax, z_rmax = r[i], z[i]
    i = np.argmin(r)
    r_rmin, z_rmin = r[i], z[i]
    i = np.argmax(z)
    r_zmax, z_zmax = r[i], z[i]
    i = np.argmin(z)
    r_zmin, z_zmin = r[i], z[i]

    a0 = (r_rmax - r_rmin) / 2.0
    r0 = (r_rmax + r_rmin) / 2.0
    cond['f_surf_95'] = (cond['f_surf']-cond['f_axis'])*0.95 + cond['f_axis']
    cond["major_radius_95"] = r0
    cond["minor_radius_95"] = a0
    cond["aspect_ratio_95"] = r0/a0
    cond["elongation_95"] = (z_zmax - z_zmin) / (r_rmax - r_rmin)
    cond["triangularity_95"] = (r0 - r_zmax) / a0
        
# x点の探索
def search_x_points(cond):
    ex = emat.dm_array(cond['xshape'])
    dz, dr = ex.dz, ex.dr
    zmin, rmin = ex.zmin, ex.rmin

    mat = cond['flux']['matrix']
    xsh = cond['xshape']['matrix'].reshape(-1)
    ir = ex.ir[xsh == 1]
    iz = ex.iz[xsh == 1]

    ipts = []
    for mz, mr in zip(iz, ir):
        dat = mat[mz-1:mz+2, mr-1:mr+2]
        if is_saddle(dat) >=4 :
            #print(f'mz: {mz}, mr:{mr}')
            ipts.append((mz, mr))
    
    # 高い低いが現れる回数で判定する場合、隣接する複数の点が検出されることがある。
    # このため、以降で、隣接する複数の点は削除して１点のみにするようにする。
    #print('ipts1: ', ipts)        
    i = 0
    while i < len(ipts):
        mz, mr = ipts[i]
        # 周囲８点の定義
        arr = [(mz-1, mr-1), (mz-1, mr), (mz-1, mr+1), 
               (mz, mr-1), (mz, mr+1), 
               (mz+1, mr-1), (mz+1, mr), (mz+1, mr+1), ]

        # 周囲８点のどれかが既にある場合、現在のデータは削除
        if any([e in arr for e in ipts]):
            ipts.pop(i)
        else:
            i += 1
    #print('ipts2: ', ipts)
    
    # メッシュ番号でなく位置を計算
    pts = []          
    for mz, mr in ipts:
        dat = mat[mz-1:mz+2, mr-1:mr+2]
        #極値と鞍点の場所は同じ方法で求められる。
        sz, sr, val = ssf.find_extremum_loc_and_val(dat)
        z = zmin + (mz + sz)*dz
        r = rmin + (mr + sr)*dr
        pts.append((r,z))    

    dic = {}
    for i, (r, z) in enumerate(pts):
        dic[f'r{i}'] = r
        dic[f'z{i}'] = z
    cond['x_points'] = dic

def is_saddle(mat):
    # ３行３列の行列を受け取り、中心の値より高い低いを判定。
    # 左下から反時計回りに評価して、８か所をres[8]に代入
    # 高い低いが現れる回数をカウントする。
    fb = mat[1, 1]
    res = [0]*8
    if mat[0, 0] > fb: # bottom, left
        res[0] = 1
    if mat[0, 1] > fb: # bottom, mid
        res[1] = 1
    if mat[0, 2] > fb: # bottom, right
        res[2] = 1
    if mat[1, 2] > fb: # mid, right
        res[3] = 1
    if mat[2, 2] > fb: # top, right
        res[4] = 1
    if mat[2, 1] > fb: # top, mid
        res[5] = 1
    if mat[2, 0] > fb: # top, left
        res[6] = 1
    if mat[1, 0] > fb: # mid, left
        res[7] = 1

    #print(res)
    
    i = 0
    while len(res) > i+1:
        if res[i] == res[i+1]:
            res.pop(i) # 高いまたは低いが連続して現れる場合は削除
        else:
            i += 1
    # len(res)>=4のとき高いが２回以上現れる。
    # つまり鞍点となる。
    return len(res)

# ストライク点の探索
def search_strike_points(cond):
    # これは、ヌル点の場所を始点として、同じ磁束を持つ場所を探索していき、
    # 真空容器にぶつかれば、そこがストライク点だとする方法
    
    flx = cond['flux']['matrix']
    fax = cond['f_surf']
    
    ir = cond['xpoint_ir']
    iz = cond['xpoint_iz']
    
    p0 = [(ir, iz)] # points
    pc = [(ir, iz)] # points to check
    
    stpoints = []
    while len(pc) != 0:
        ir, iz = pc.pop(0) # 探索すべき中心点

        # 周辺８点の定義
        points = [
            (ir+1, iz), (ir-1, iz), (ir, iz+1), (ir, iz-1), 
            (ir+1, iz+1), (ir+1, iz-1), (ir-1, iz+1), (ir-1, iz-1),
            ]
        
        for e in points:
            inr, inz = e[0], e[1]
            if not e in p0 and (flx[iz, ir]-fax)*(flx[inz, inr]-fax) <= 0.0:
                # 新しい点であり、最外殻磁気面の値と交差している場合
                if 1 == cond['vessel']['matrix'][inz, inr]:
                    # かつ、その点が真空容器の中の場合、新しい点を追加してループ継続
                    p0.append(e)
                    pc.append(e)
                    continue
                
                # 新しい点が真空容器上の場合は、そこがストライク点
                if not e in stpoints:
                    stpoints.append(e)
        
    xfig = copy.deepcopy(cond['domain'])
    xfig['matrix'] = np.zeros(xfig['matrix'].shape)
    for e in p0:
        xfig['matrix'][e[1], e[0]] = 1
    cond['xshape'] = xfig
    
    # print(stpoints)
    
    # ここまででヌル点を始点として真空容器壁まで到達した点が分かる。
    # ただ、その点の近傍も含んでいる可能性があるの、ここで、
    # 接触しているポイントは、一つにまとめる。
    def get_one_group(pts):
        p0 = pts.pop(0)
        pg = {p0} # チェックすべき点
        gr = {p0} # ひとまとまりのグループ
        while len(pg) != 0:
            ix, iy = pg.pop()
            # 上下左右4個所
            dir = [(ix+1, iy), (ix-1, iy), (ix, iy+1), (ix, iy-1)]
            for e in dir:
                if e in pts:
                    nep = pts.pop(pts.index(e))
                    pg.add(nep)
                    gr.add(nep)
        return gr, pts    

    #print('stpoints: ', stpoints)
    
    # indexで平均を取るとズレが大きくなるので、
    # 座標に戻してから平均をとる。
    rmin, dr = xfig['rmin'], xfig['dr']
    zmin, dz = xfig['zmin'], xfig['dz']
    stponew = []
    while len(stpoints) != 0:
        og, stpoints = get_one_group(stpoints)
        # print(og)
        # print(stpoints)
        a = np.array([p for p in og]) # 集合をnumpyに変換
        # 座標に変換
        d = np.array([(rmin + dr*e[0], zmin + dz*e[1]) for e in a])
        b = np.average(d, axis=0) # 点群の位置の平均
        stponew.append(tuple(b))
    #print('s', stponew)

    # indexからr, z を求めるより、search_strike_points_surfの方が正確
    # なので、前者の値に最も近い後者を採用することにする。
    ans = []
    pts = search_strike_points_surf(cond)
    
    if 0 == len(pts):
        cond["cal_result"] = -1
        cond["error_messages"] += 'Can not find strike points.'
        return

    for ip in stponew:
        r, z = ip
        lensq = [((sr-r)**2 + (sz -z)**2) for sr, sz in pts]
        # 距離が最小となるindex
        idx = np.argmin(np.array(lensq))
        #print('dist: ', lensq[idx])
        if lensq[idx] < dr + dz:
            # 最小となる距離がメッシュの長さ程度に収まっていたら追加
            ans.append(pts[idx])
    
    dic = {}
    for i, (r, z) in enumerate(ans):
        dic[f'r{i}'] = r
        dic[f'z{i}'] = z
    cond['strike_points'] = dic
        
def search_strike_points_surf(cond): 
    # これは、真空容器の表面で、最外殻磁気面と同じ値の磁束である場所を探す方法
    # この方法の単独の使用はあまりよくない。最外殻磁気面の磁力線が、
    # 真空容器内外を行ったり来たりする場合に、多くのストライク点があると判断してしまう。
    
    # 真空容器壁の座標を取得
    g = emat.dm_array(cond['vessel'])
    stepw = np.min([g.dr, g.dz])/2.0 # 座標探索の幅は、メッシュサイズより小さくする。
    points = np.array([[0, 0]]) # ダミーデータ
    for i in range(len(gl.vessel_points)-1):
        p0 = np.array(gl.vessel_points[i])
        p1 = np.array(gl.vessel_points[i+1])
        length = np.sqrt(np.sum((p1-p0)**2))
        points = np.concatenate([points, np.linspace(p0, p1, int(length/stepw))[:-1]])
    points = points[1:] # ダミーは削除

    # 座標におけるフラックスを取得
    fl = [emat.linval2(p[0], p[1], cond['flux']) for p in points]

    stp = []
    for i in range(len(fl)-1):
        v1 = fl[i]-cond['f_surf']
        v2 = fl[i+1]-cond['f_surf']
        if v1*v2 < 0: # 積が負の場合は、そこがストライク点
            v1, v2 = np.abs(v1), np.abs(v2)
            # v1, v2の比に応じて座標を計算。ex. v1がほぼゼロの時は、ほぼpoint[i]になる。
            stp.append(points[i] + (points[i+1]-points[i])*(v1/(v1+v2)))
            #print(fl[i], fl[i+1], cond['f_surf'])
            #print(points[i], points[i+1], points[i] + (points[i+1]-points[i])*(v1/(v1+v2)))
    
    # dst = {}
    # for i, e in enumerate(stp):
    #     dst[f'r_p{i}'] = e[0]
    #     dst[f'z_p{i}'] = e[1]
    # cond['strike_points'] = dst
    return stp
    
# ベータ値の計算
def calc_beta(cond):
    u0 = sc.mu_0  # 真空の透磁率
    pi = sc.pi

    # 圧力の体積平均
    pv = get_volume_average(cond["pressure"], cond["domain"])
    cond["pressure_average_volume"] = pv
    # factor *2 means ion and electro
    #cond["stored_energy"] = (3/2)*pv*cond["volume"]*2
    cond["stored_energy"] = (3/2)*pv*cond["volume"]
        
    # 圧力の面積平均
    ps = get_cross_section_average(cond["pressure"], cond["domain"])
    cond["pressure_average_section"] = ps
    
    # 磁気軸におけるトロイダル磁場
    # 2 pi R Bt = mu0 I
    ir_ax, iz_ax = cond["axis_ir"], cond["axis_iz"]
    r_ax = cond["axis_r"]
    # プラズマ無しのトロイダル磁場
    bt0 = u0 * cond["cur_tf"]['tf']*cond["cur_tf"]["turn"]
    bt0 /= (2 * pi * r_ax)
    # プラズマ込みのトロイダル磁場
    polcur = cond["pol_current"]["matrix"][iz_ax, ir_ax]
    bt = u0 * polcur / (2 * pi * r_ax)

    # poloidal beta bp = (8 pi <p> S)/(u0 Ip^2)
    ip = cond["cur_ip"]["ip"] # ip
    s = cond["cross_section"] # cross section area
    a = np.sqrt(s/pi) # minor radius
    cond["beta_poloidal"] = (8*pi*ps*s)/(u0*ip*ip) # ps: section average

    # toroidal beta bet_tor = <p>/(bt0^2/(2u0))
    betr = pv / (bt**2 / 2 / u0) # pv: volume average
    cond["beta_toroidal"] = betr

    #  normalized beta
    ipma = ip/(10**6) # unit: MA
    betrper = betr*100 # unit: [%], percent
    betnor = np.abs(betrper*a*bt/ipma) # bt: toroidal with plasma
    cond["beta_normalized"] = betnor
    
    return cond

# インダクタンスの計算
def calc_inductance(cond):
    g = emat.dm_array(cond["domain"])
    d = cond["domain"]["matrix"].reshape(-1)
    br = cond['br_plasma']["matrix"].reshape(-1)
    bz = cond['bz_plasma']["matrix"].reshape(-1)
    
    # domainの場所だけ取り出す。
    br = br[d == 1]
    bz = bz[d == 1]
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    nr, nz = g.nr, g.nz
        
    bt2 = br*br + bz*bz
    bt2mat = np.zeros((nz, nr))
    for v, i, j in zip(bt2, ir, iz):
        bt2mat[j, i] = v
        
    bt2jt = copy.deepcopy(gl.get_dmat_coarse())
    bt2jt["matrix"] = bt2mat
    cond["b_theta_jt_square"] = bt2jt
    
    #print('volume average', get_volume_average(bt2jt, cond['domain']))
    #print('section average', get_cross_section_average(bt2jt, cond['domain']))

    # normalized internal inductance
    # vn: <b_theta_square>_(section_average)
    # vd: b_theta(a)^2    
    vn = get_cross_section_average(bt2jt, cond['domain'])
    vd = (4.0*np.pi*10**(-7)*cond['cur_ip']['ip'])**2/(4*np.pi*cond['cross_section'])
    cond['inductance_internal_normalized'] = vn/vd
    
    # フラックスを算出。プラズマ由来であることに注意
    pr, pz = cond['pts']['r_rmin'], cond['pts']['z_rmin']
    fxe = np.abs(emat.linval2(pr, pz, cond['flux_plasma'])) # 最外殻磁気面におけるフラックス
    # ピーク位置のフラックス。磁気軸位置とは一致しないことに注意。
    fxt = np.max(np.abs(cond['flux_plasma']['matrix'])) # プラズマが存在することによる全磁束 
    fxi = fxt - fxe # プラズマ内部に存在するフラックス
    ip = cond['cur_ip']['ip']

    cond['inductance_internal'] = fxi/ip
    cond['inductance_self'] = fxt/ip
    
    vn = get_volume_average(bt2jt, cond['domain'])*cond['volume']
    vd = (4.0*np.pi*10**(-7))*(cond['cur_ip']['ip']**2)
    cond['inductance_internal_btheta'] = vn/vd
    
    return cond

# safety factorの計算（多項式近似その１）
def calc_safety_poly(cond):
    # トロイダルフラックスをポロイダル電流から計算
    # トロイダルフラックスを多項式で近似
    # その多項式関数の微分をとって安全係数を算出
    
    # この方法は微分を取った後の関数が中心ピークでなかったりするのが問題
    
    poly = 3 # 近似する多項式の次数

    g = emat.dm_array(cond["domain"])
    d = cond["domain"]["matrix"].reshape(-1)
    f = cond["flux_normalized"]["matrix"].reshape(-1)
    p = cond["pol_current"]["matrix"].reshape(-1)

    # domainの場所だけ取り出す。
    f = f[d == 1]
    p = p[d == 1]
    r = g.r[d == 1]
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    nr, nz = g.nr, g.nz

    # bfの磁気面内の面積分を行う。
    # bf: トロイダル方向の磁束密度
    # 2 pi r bf = u0 I, thus bf = 2.0e-7 * I / r
    func = 2.0 * 10 ** (-7) * p / r
    func *= (g.dr*g.dz) # 面積積分なのでメッシュ面積をかける。

    x = np.linspace(0, 1, 11)
    y = [np.sum(func[f <= e]) for e in x]
    cond['toroidal_flux'] = y
    
    coef = np.polyfit(x, y, poly) # [3次の係数, 2次係数, 1次係数, 0次係数]
    val = [poly-e for e in range(poly+1)] # [3, 2, 1, 0]
    coef *= val
    coef = coef[:-1] # 多項式関数を微分した係数
    
    cond['toroidal_flux_diff'] = np.poly1d(coef)(x)
    
    # 安全係数(分布)の計算
    fax, fbn = cond["f_axis"], cond["f_surf"]
    q = np.poly1d(coef)(f)/(fbn - fax)
    
    qmat = np.zeros((nz, nr))
    for v, i, j in zip(q, ir, iz):
        qmat[j, i] = v
        
    safety = copy.deepcopy(gl.get_dmat_coarse())
    safety["matrix"] = qmat
    cond["safety_factor"] = safety        
    
    # 安全係数（正規化フラックス）の計算
    f0 = np.linspace(0.0, 1.0, nr)
    q0 = np.poly1d(coef)(f0)/(fbn - fax)

    cond["safety_factor_norm"] = q0
    
    return cond    

# safety factorの計算（多項式近似その２）
def calc_safety_poly2(cond):
    # トロイダルフラックスをポロイダル電流から計算
    # その微分を計算
    # その微分値に対して多項式近似を行う。
    
    g = emat.dm_array(cond["domain"])
    d = cond["domain"]["matrix"].reshape(-1)
    f = cond["flux_normalized"]["matrix"].reshape(-1)
    p = cond["pol_current"]["matrix"].reshape(-1)

    # domainの場所だけ取り出す。
    f = f[d == 1]
    p = p[d == 1]
    r = g.r[d == 1]
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    nr, nz = g.nr, g.nz

    poly = 4 # 多項式の次数
    #num = 61 # 微分を算出するときのサンプリング数
    num = g.nr # 微分を算出するときのサンプリング数
    width = 5 # 移動平均の幅
    ckernel = np.ones(width)/width # 畳み込み積分のカーネル
    
    # bfの磁気面内の面積分を行う。
    # bf: トロイダル方向の磁束密度
    # 2 pi r bf = u0 I, thus bf = 2.0e-7 * I / r
    func = 2.0 * 10 ** (-7) * p / r
    func *= (g.dr*g.dz) # 面積積分なのでメッシュ面積をかける。

    x = np.linspace(0, 1, num)
    y = [np.sum(func[f <= e]) for e in x]

    cond['toroidal_flux'] = y
    dy = np.diff(y)/(x[1]-x[0]) # 微分を計算
    dy = np.append(dy[0], dy) # 要素数を揃える。
    cond['toroidal_flux_diff'] = dy    
    
    # この toroidal_flux_diff は差分を取っているのでガタガタしている。
    # したがって移動平均を取って平滑化する。
    dy = np.convolve(dy, ckernel, mode='same')     
    cond['toroidal_flux_diff'] = dy

    # 微分した値に関する多項式近似
    coef = np.polyfit(x, dy, poly) # [3次の係数, 2次係数, 1次係数, 0次係数]

    # 安全係数(分布)の計算
    fax, fbn = cond["f_axis"], cond["f_surf"]
    q = np.poly1d(coef)(f)/(fbn - fax)
    
    qmat = np.zeros((nz, nr))
    for v, i, j in zip(q, ir, iz):
        qmat[j, i] = v
        
    safety = copy.deepcopy(gl.get_dmat_coarse())
    safety["matrix"] = qmat
    cond["safety_factor"] = safety        
    
    # 安全係数（正規化フラックス）の計算
    f0 = np.linspace(0.0, 1.0, nr)
    q0 = np.poly1d(coef)(f0)/(fbn - fax)

    cond["safety_factor_norm"] = q0
    
    return cond            
    
# safety factorの計算
def calc_safety(cond):
    # calc toroidal flux
    # ft = Integrate_area[u0*I/(2*pi*r)]
    g = emat.dm_array(cond["domain"])
    d = cond["domain"]["matrix"].reshape(-1)
    f = cond["flux_normalized"]["matrix"].reshape(-1)
    p = cond["pol_current"]["matrix"].reshape(-1)

    # domainの場所だけ取り出す。
    f = f[d == 1]
    p = p[d == 1]
    r = g.r[d == 1]
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    nr, nz = g.nr, g.nz

    # bfの磁気面内の面積分を行う。
    # bf: トロイダル方向の磁束密度
    # 2 pi r bf = u0 I, thus bf = 2.0e-7 * I / r
    func = 2.0 * 10 ** (-7) * p / r
    func *= (g.dr*g.dz) # 面積積分なのでメッシュ面積をかける。

    x = np.linspace(0, 1, 11)
    y = [np.sum(func[f <= e]) for e in x]

    cond['toroidal_flux'] = y
    dy = np.diff(y)/(x[1]-x[0]) # 微分を計算
    dy = np.append(dy[0], dy) # 要素数を揃える。
    cond['toroidal_flux_diff'] = dy
    
    fnc = interp1d(x, dy, kind='cubic') 
    fax, fbn = cond["f_axis"], cond["f_surf"]
    #print(f'fmax:{max(f)}, fmin:{min(f)}, xmin:{x[0]}, xmax:{x[-1]}')
    q = fnc(f)/(fbn - fax)

    qmat = np.zeros((nz, nr))
    for v, i, j in zip(q, ir, iz):
        qmat[j, i] = v

    safety = copy.deepcopy(gl.get_dmat_coarse())
    safety["matrix"] = qmat
    cond["safety_factor"] = safety

    # 正規化フラックスに応じた値の計算
    f0 = np.linspace(0.0, 1.0, nr)
    q0 = fnc(f0)/(fbn - fax)

    cond["safety_factor_norm"] = q0
    
    return cond

def constraints_stored_energy(cond, const_stored):
    g = emat.dm_array(cond['domain'])
    dr, dz = g.dr, g.dz
    ds = dr * dz
    faxis, fsurf = cond["f_axis"], cond["f_surf"]
    npr = cond["num_dpr"]
    ncu = cond["num_di2"]
    
    d = cond['domain']['matrix'].reshape(-1)
    n = cond['flux_normalized']['matrix'].reshape(-1)
    
    # domain内のみ考える。
    r = g.r[d == 1]
    n = n[d == 1]

    # 各メッシュにおける圧力×微小円環体積
    pv = [2 * np.pi * r * ds * (fsurf-faxis)*((n**(i+1)-1)/(i+1) - (n**(npr+1)-1)/(npr+1)) for i in range(npr)]
    pv = np.array(pv).T #[points, num of coef]
    pv = np.sum(pv, axis=0) # [num of coef]
    pv = list(pv) + [0]*ncu # [num of total coef]
    pv = np.array(pv).reshape(1, -1)
    
    #volume = np.sum(2 * np.pi * r * ds)
    
    # stored energy = 3 <p>*vol (<p> is volume average.)
    # Here, pv is same to <p>*vol.
    # factor *2 means ion and electron
    #arr = (3/2)*pv*2  
    arr = (3/2)*pv
        
    for key in const_stored.keys():
        val = [const_stored[key]['stored_energy']]
        wgt = [const_stored[key]['weight']**2] # squared
  
    val = np.array(val)
    wgt = np.array(wgt)      
    #print('arr', arr, ' val', val, ' wgt', wgt)
    return arr, val, wgt  

# 束縛条件_jt
def constraints_jt(cond, const_jt):
    npr = cond["num_dpr"]
    ncu = cond["num_di2"]
    dm_nf = cond['flux_normalized']
    dm_domain = cond['domain']
    
    array_cf = [] # array of coef
    array_jt = [] # array of pressure
    array_wt = [] # array of weighting factor
    
    cojt = const_jt
    for e in cojt.keys():
        rp, zp = cojt[e]['point']

        # domainの外は無視
        if 0 == emat.linval2(rp, zp, dm_domain):
            continue

        # pointにおけるnormalized flux
        nfp = emat.linval2(rp, zp, dm_nf) 
        # 圧力由来のjt = 2*pi*r * (dp/df)
        p0 = [2.0 * np.pi * rp * (nfp**i - nfp**npr) for i in range(npr)]
        # ポロイダル電流由来: jt = 10**(-7)/r * (di^2/df)
        p1 = [10**(-7)/(rp + 10**(-7)) * (nfp**i - nfp**ncu) for i in range(ncu)]
        
        array_cf.append(p0+p1)
        array_jt.append(cojt[e]['jt'])
        array_wt.append(cojt[e]['weight']**2) # squared
        
    # np arrayに変換。この時点で、[point数、パラメータ数]
    array_cf = np.array(array_cf)
    array_jt = np.array(array_jt)
    array_wt = np.array(array_wt)        
    
    return array_cf, array_jt, array_wt
        
# 束縛条件_圧力
def constraints_pressure(cond, const_pressure):
    #print(const_pressure)
    faxis, fsurf = cond["f_axis"], cond["f_surf"]
    npr = cond["num_dpr"]
    ncu = cond["num_di2"]
    dm_nf = cond['flux_normalized']
    dm_domain = cond['domain']

    array_cf = [] # array of coef
    array_pr = [] # array of pressure
    array_wt = [] # array of weighting factor

    copr = const_pressure
    for e in copr.keys():
        if copr[e]['point'] == 'axis':
            rp = cond['axis_r']
            zp = cond['axis_z']
        else:
            rp,zp = copr[e]['point']

        # domainの外は無視
        if 0 == emat.linval2(rp, zp, dm_domain):
            continue

        # pointにおけるnormalized flux
        nfp = emat.linval2(rp, zp, dm_nf) 
        # onp[npr]の形
        onp = [(fsurf-faxis)*((nfp**(i+1)-1)/(i+1) - (nfp**(npr+1)-1)/(npr+1)) for i in range(npr)]
        onp += [0]*ncu # onp[npr + ncu] :パラメータの数

        array_cf.append(onp)
        array_pr.append(copr[e]['pressure'])
        array_wt.append(copr[e]['weight']**2) # squared
    
    # np arrayに変換。この時点で、[point数、パラメータ数]
    array_cf = np.array(array_cf)
    array_pr = np.array(array_pr)
    array_wt = np.array(array_wt)
    #print('cf', array_cf.shape, array_pr.shape, array_wt.shape)
    #print(array_wt)

    return array_cf, array_pr, array_wt

# 束縛条件_Ip
def constraint_ip(cond, mat_jt, constrain):
    
    dm_domain = cond['domain']
    g = emat.dm_array(dm_domain)
    ds = g.dr * g.dz # メッシュ面積
    
    # mat_jt[num of plasma points, num of coefs]
    arr = np.sum(mat_jt, axis=0).reshape(1, -1)*ds # [[num of coef]]
    
    for key in constrain.keys():
        val = [constrain[key]['ip']]
        wgt = [constrain[key]['weight']**2] # squared
    val = np.array(val)
    wgt = np.array(wgt)
    #print('ip: arr', arr, 'val', val, 'wgt', wgt)
    return arr, val, wgt

# 束縛条件_flux, br, bz
def constraints_mag(cond, arr_f, syurui, constraints):
    #arr_f[num of plasma points, num of fitting param]
    # syurui: 'flux', 'br', 'bz'

    cofl = constraints
    # 最初に種類に応じて変数をセット
    if syurui == 'flux':
        d_mat = cond['flux_coil']
        basemat = pmat
    elif syurui == 'br':
        d_mat = cond['br_coil']
        basemat = pmat_br
    elif syurui == 'bz':
        d_mat = cond['bz_coil']
        basemat = pmat_bz

    dm_domain = cond['domain']
    d = dm_domain['matrix'].reshape(-1)

    g = emat.dm_array(dm_domain)
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    ds = g.dr * g.dz # メッシュ面積

    arr = []
    val = []
    wgt = []
    for e in cofl.keys():
        rf, zf = cofl[e]['point']
        # コイル由来の値
        fxc = emat.linval2(rf, zf, d_mat)
        #print('fxc', fxc)
        # 拘束条件位置のメッシュ番号
        nrf, nzf = gl.get_coarse_grid_num(rf, zf)
        hmat = [
            basemat.cget(nzp, nrp)[nzf, nrf] for nrp, nzp in zip(ir, iz)
        ]
        # jは電流密度を想定しているのに対して、pmat.cgetは単位電流
        # 電流密度に変更するためにdsをかけておく必要がある。
        hmat = np.array(hmat)
        hmat = np.diag(hmat)
        b = np.sum(np.dot(hmat, arr_f), axis=0)
        arr.append(b)
        val.append(cofl[e][syurui]-fxc) # コイル由来の値は除いておくこと
        
        wgt.append(cofl[e]['weight']**2) # squared
    arr = np.array(arr)*ds
    val = np.array(val)
    wgt = np.array(wgt)
    # print('arr', arr, 'val', val, 'wgt', wgt)
    # print(arr.shape, val.shape, wgt.shape)
    return arr, val, wgt

# フィッティング係数を求めるためのプラズマ電流密度のマトリックスを作成
def make_initial_matrix(cond):

    # 下の式の、Fとj0を作成する。
    # j1 = F c
    # j0

    # 各matrixの取得
    dm_jt = cond["jt"]
    dm_nf = cond["flux_normalized"]
    dm_domain = cond["domain"]

    # 時数の取得
    npr = cond["num_dpr"]
    ncu = cond["num_di2"]

    g = emat.dm_array(dm_domain)

    # 一次元化
    f = dm_nf["matrix"].reshape(-1)
    j = dm_jt["matrix"].reshape(-1)
    d = dm_domain["matrix"].reshape(-1)

    # この時点での電流の総和を取得
    jtotal = j.sum()
    
    # 最外殻磁気面の内部のみ取り出す。
    r = g.r[d == 1]
    j = j[d == 1]
    f = f[d == 1]

    js = j.sum()
    if js == 0.0:
        cond['cal_result'] = -1
        cond['error_messages'] += 'Sum of jt become zero in make_initial_matrix.\n'
        return -1, -1
        
    # 磁気面内の内部のみ取り出した場合、電流の総和が変化する場合がある。
    # これを避けるため、電流の総和を最初の値にする。
    j *= jtotal/js

    # 例えばパラメータ数が３の場合の時
    # (1-x^3) *a0 + (x^1-x^3)*a1 + (x^2-x^3)*a2
    # という形になることに注意すること

    # 圧力に関する行列作成 [npr+1, ポイント数]
    # 圧力由来のjt = 2*pi*r * (dp/df)
    p0 = np.array([2 * np.pi * r * (f**i - f**npr) for i in range(npr)])
    
    # I^2に関する行列作成 [ncu+1, ポイント数]
    # ポロイダル電流由来: jt = 10**(-7)/r * (di^2/df)
    p1 = np.array(
        [10 ** (-7) / (r + 10 ** (-7)) * (f**i - f**ncu) for i in range(ncu)]
    )
    
    # 結合させて転置、この時点で[point数, パラメータ数]の形
    a = np.vstack([p0, p1]).transpose() # matrix

    return a, j    

# 束縛条件のマトリックスをオリジナルにスタックしていく
def const_stack_matrix(mat0, val0, wgt0, mat1, val1, wgt1, j0):
    # mat0, val0, wgt0: original
    # mat1, val1, wgt1: additional_constraints
    # trimming weight with j0 (initial plasma current)

    j0_ave = np.average(np.abs(j0))
    num = len(j0)
    val1_ave = np.average(np.abs(val1))
    
    if 0.0 == val1_ave:
        val1_ave = 1.0
    
    tw = (j0_ave/val1_ave)**2 # trimming factor for weight_squared
    wgt1 *= tw # 値が同等になるためのweightの補正
    wgt1 *= num # j0は式の数が多いので、式の数に対する補正

    a1 = np.vstack((mat0, mat1)) 
    v1 = np.append(val0, val1)
    w1 = np.append(wgt0, wgt1)
    
    return a1, v1, w1

def constraints_procedure(cond, mat0, val0, wgt0, mat_jt, jt):
    # mat_jt : matrix derived from only jt. [num of plasma points, num of coefs]
    # jt : jt
    a1 = copy.copy(mat0)
    v1 = copy.copy(val0)
    w1 = copy.copy(wgt0)

    if 'constraints' not in cond.keys():
        return a1, v1, w1
    
    # constraintsを種類ごとに分ける
    const_pressure  = {}
    const_jt        = {}
    const_stored    = {}
    const_ip        = {}
    const_flux      = {}
    const_br        = {}
    const_bz        = {}
    
    dic = cond['constraints']
    for e in dic.keys():
        if 'pressure' in dic[e].keys():
            const_pressure[e] = dic[e]
        if 'jt' in dic[e].keys():
            const_jt[e] = dic[e]
        if 'stored_energy' in dic[e].keys():
            const_stored[e] = dic[e]
        if 'ip' in dic[e].keys():
            const_ip[e] = dic[e]
        if 'flux' in dic[e].keys():
            const_flux[e] = dic[e]
        if 'br' in dic[e].keys():
            const_br[e] = dic[e]
        if 'bz' in dic[e].keys():
            const_bz[e] = dic[e]
    
    # 束縛条件が存在していれば、それをスタック
    if len(const_pressure) != 0:
        mat, val, wgt = constraints_pressure(cond, const_pressure)
        if len(mat) != 0:
            # 全ての座標がプラズマ外の場合は、束縛条件を実行できない。
            a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt)
        
    if len(const_jt) != 0:
        mat, val, wgt = constraints_jt(cond, const_jt)
        a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt)
        
    if len(const_stored) != 0:
        mat, val ,wgt = constraints_stored_energy(cond, const_stored)
        a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt)
        
    if len(const_ip) != 0:
        mat, val, wgt = constraint_ip(cond, mat_jt, const_ip)
        a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt)
        
    if len(const_flux) != 0:
        mat, val, wgt = constraints_mag(cond, mat_jt, 'flux', const_flux)
        a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt)        

    if len(const_br) != 0:
        mat, val, wgt = constraints_mag(cond, mat_jt, 'br', const_br)
        a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt) 
        
    if len(const_bz) != 0:
        mat, val, wgt = constraints_mag(cond, mat_jt, 'bz', const_bz)
        a1, v1, w1 = const_stack_matrix(a1, v1, w1, mat, val, wgt, jt) 
    
    return a1, v1, w1

# 1次元配列をd_matに変換
def conv_1d_array_to_dmat(cond, array):
    dm_domain = cond['domain']
    g = emat.dm_array(dm_domain)
    d = dm_domain["matrix"].reshape(-1)
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]

    mat = np.zeros((g.nz, g.nr))
    for i, j, v in zip(ir, iz, array):
        mat[j, i] = v
    res = emat.get_dmat_dim(dm_domain)
    res['matrix'] = mat
    return res

# 固定境界が設定されている場合の事前行列の計算
def fix_boundary_calc_initial_mat(cond):
    # ex. 使用コイルが３つであって、固定境界点が５つの場合
    # できあるmat2は、[3, 5]の形である。
    # これに、それぞれの個所の（フラックス-最外殻磁気面磁束)をかけると
    # コイル電流の増減量が分かる。
    
    # 使用しているコイル一覧　sortedをかけて並べ替えている。
    pfc = sorted(cond['cur_pf'].keys())
    # 最外殻磁気面のポイント sortedをかけて並べ替えている。
    flp = sorted(cond['fix_boundary'].keys())

    # \Phi_{mn}: 基本行列の作成
    mat = []
    for pc in pfc:
        onecoil = cmat.get_flux_of_coil({'cur_pf':{pc:1.0}})
        oc = []
        for fp in flp:
            r, z = cond['fix_boundary'][fp]['point']
            oc.append(emat.linval2(r, z, onecoil))
        mat.append(oc)
    mat = np.array(mat).T
    
    # 重み行列の作成
    wmt = []
    for fp in flp:
        w = cond['fix_boundary'][fp]['weight']
        wmt.append(w**2) # 重み行列の要素は重みの２乗
    wmt = np.diag(np.array(wmt))
    
    #mat1 =np.dot(mat.T, mat)
    #mat2 = np.dot(np.linalg.inv(mat1), mat.T)
    mat1 =mat.T @ wmt @ mat
    mat2 = np.linalg.inv(mat1) @ ((wmt @ mat).T)
    cond['fix_boundary_mat'] = mat2

# 固定境界が設定されている場合のPF電流の調節
def fix_boundary_adjust_pf(cond):
    # 最外殻磁気面のポイント
    flp = sorted(cond['fix_boundary'].keys())
    oc = []
    for fp in flp:
        r, z = cond['fix_boundary'][fp]['point']
        val = cond['fix_boundary'][fp]['val']
        
        av = cond['f_surf']
        if val != 'lcfs':
            av = cond['f_surf_95']
            
        fl = emat.linval2(r, z, cond['flux'])
        oc.append(fl - av)

    delcurs = np.dot(cond['fix_boundary_mat'], oc) # コイル電流の増減
  
    # 使用しているコイル一覧　sortedをかけて並べ替えている。
    pfc = sorted(cond['cur_pf'].keys())
    
    # 変化率の係数算出, シグモイド関数による係数。
    # 最初の内は小さな係数で、iterを繰り返すうちに1に近づいていく係数
    # 15回目のiterで0.5になる計算式
    # 収束先が不安定（高い圧力など）の場合、
    # 係数が１だと計算も不安定になる可能性有り？ので無効にしておく
    # i = cond['iter']
    # c = 1/(1 + np.e**(-0.1*(i-15)))
        
    # PF電流の調整 cur*1.0だと振動する？
    for pf, cur in zip(pfc, delcurs):
        cond['cur_pf'][pf] -= cur * 0.1
        #cond['cur_pf'][pf] -= cur * c
        
    # print(cond['cur_pf'])
    # print(delcurs)
    #print('error', np.dot(oc, oc), cond['f_surf_95'])
    
# iterationをする過程でチェックする値の確認
def check_monitor_value(cond):
    # monitor_valuesが設定されていたら、それを保存する
    if 'monitor_values' not in cond.keys():
        return
    
    #　'monitor_result'がない場合の初期化
    if 'monitor_result' not in cond.keys():
        cond['monitor_result'] = {}
        for e in cond['monitor_values']:
            cond['monitor_result'][e] = []
    # 値の記録
    for e in cond['monitor_values']:
        #if e in cond.keys() and isinstance(cond[e], (int, float)):
        if e in cond.keys():
            cond['monitor_result'][e].append(cond[e])    

def conv_monitor_value_to_str(cond):
    st = ""
    if 'monitor_values' not in cond.keys():
        return st
    
    def trim_val(name, val, st):
        if isinstance(val, (int, float)):
            st += f'{name}: {val:.2e}, '
        elif type(val) == dict:
            for e in val.keys():
                st = trim_val(e, val[e], st)
        return st
    
    for e in cond['monitor_values']:
        if e in cond.keys():
            st = trim_val(e, cond[e], st)
    
    st = ', ' + st[:-2]
    return st
                   
# 平衡計算条件の事前チェック_領域内に設定されているか確認
def precheck_of_condition_sub(cond, key):
    if not key in cond:
        return
    
    rmin, rmax = gl.cr_min, gl.cr_max
    zmin, zmax = gl.cz_min, gl.cz_max
    
    val = cond[key]
    for e in val.keys():
        r, z = val[e]
        if r < rmin or rmax < r or z < zmin or zmax < z:
            cond["cal_result"] = -1
            cond["error_messages"] += f"Position is out of range: {key}.{e}\n"    
    
# 平衡計算条件の事前チェック
def precheck_of_condition(cond):
    rmin, rmax = gl.cr_min, gl.cr_max
    zmin, zmax = gl.cz_min, gl.cz_max

    keys = ["fl_pos", "br_pos", "bz_pos"]
    for k in keys:
        precheck_of_condition_sub(cond, k)

    if "constraints" in cond:
        dic = cond["constraints"]
        for k in dic.keys():
            # Ipに関する束縛条件
            if 'ip' in dic[k].keys():
                # IPの束縛条件が存在しているときIpは可変
                cond['fix_cur'] = False
            else:
                # 束縛条件がないときはIp固定
                cond['fix_cur'] = True
            
            # constraintsで指定されている点が少なくとも領域内にあるかを確認
            if 'point' in dic[k].keys():
                if 'axis' == dic[k]["point"]:
                    continue # 'point'は'axis'の場合あり
                r, z = dic[k]["point"]
                if r < rmin or rmax < r or z < zmin or zmax < z:
                    cond["cal_result"] = -1
                    cond["error_messages"] += f"Constrain point is out of range: {k}\n" 
                
# 平衡計算の前処理
def equi_pre_process(condition, verbose=3):
    cond = copy.deepcopy(condition)
    
    # default値、または初期値の設定
    cond['error_messages'] = ""
    cond["error"] = []
    cond["cal_result"] = 0
    cond["bad_step"] = 0
    if not 'bad_step_max' in cond.keys():
        cond['bad_step_max'] = 4
    
    if not 'conv_judge_val' in cond.keys():
        cond['conv_judge_val'] = 0.05
        
    if not 'num_dpr' in cond.keys():
        cond['num_dpr'] = 1
    if not 'num_di2' in cond.keys():
        cond['num_di2'] = 1
    
    # 真空容器
    dm_vv = vmat.get_vessel(cond)
    cond["vessel"] = dm_vv

    g = emat.dm_array(dm_vv)
    # ポロイダル電流
    dm = emat.get_dmat_dim(dm_vv)
    dm['matrix'] = np.ones((g.nz, g.nr))*cond["cur_tf"]["tf"]*cond["cur_tf"]["turn"]
    cond['pol_current'] = dm
    
    # TFコイル巻き戻しのチェック
    cond = ssf.check_tf_rewind(cond)
    
    # プラズマ電流
    # 'jt'が定義されている場合はそれを使用する。
    if not 'jt' in cond.keys():
        cond["jt"] = pmat.d_set_plasma_parabolic(cond)

    # 磁場クラスの作成
    mag = smc.Magnetic(cond)

    # コイルによるフラックス, br, bz
    cond["flux_coil"] = mag.fl_c
    cond["br_coil"] = mag.br_c
    cond["bz_coil"] = mag.bz_c
    cond["bt_coil"] = mag.bt_c

    # プラズマ電流によるフラックス,br, bz
    cond["flux_plasma"] = mag.fl_p
    cond["br_plasma"] = mag.br_p
    cond["bz_plasma"] = mag.bz_p
    cond["bt_plasma"] = mag.bt_p

    # トータルのフラックス, br, bz
    cond["flux"] = mag.fl
    cond["br"] = mag.br
    cond["bz"] = mag.bz
    cond["bt"] = mag.bt

    # 最外殻磁気面
    dm_dm = search_domain(cond)
    cond["domain"] = dm_dm

    # 領域の中心位置におけるコイルフラックスの値取得
    # r, z = gl.vessel_center # 真空容器の中心の場合
    r, z = cond['cur_ip']['r0'], cond['cur_ip']['z0'] # プラズマ初期位置の場合

    f = emat.linval2(r, z, cond["flux_coil"])
    ip = cond["cur_ip"]["ip"]
    # ipとfの積が正の場合は平衡が成り立たないので除外する。
    if 0 < f * ip:
        #cond["domain"] = None
        cond["cal_result"] = -1
        cond["error_messages"] += 'Invalid direction of plasma current.'
    
    # ecrの周波数が設定されていたら計算
    set_ecr_position(cond)
    
    # 固定境界が設定されていたら行列を事前に作成
    if 'fix_boundary' in cond.keys():
        fix_boundary_calc_initial_mat(cond)
        
    # 平衡計算条件の事前チェック
    precheck_of_condition(cond)

    display_information(cond, verbose=verbose, proc='pre-process')
                
    return cond

def equi_post_proc_pressure_and_pol_current(cond):
    # プラズマ圧力とポロイダル電流の行列計算
    
    g = emat.dm_array(cond['domain'])
    
    # 圧力微分dp/dfと圧力pの計算
    dm_dp, dm_pr = get_dpress_press(cond)
    cond["diff_pre"] = dm_dp
    cond["pressure"] = dm_pr
    
    # 規格化フラックスでの圧力微分dp/dfと圧力pの計算
    dp, pr = get_dpress_press_normal(cond, g.nr)
    cond['diff_pre_norm'] = dp
    cond['pressure_norm'] = pr

    # ポロイダル電流微分di^2/dfとポロイダル電流の計算
    dm_di2, dm_polcur = get_di2_i(cond)
    cond["diff_i2"] = dm_di2
    cond["pol_current"] = dm_polcur
    
    # 規格化フラックスでの電流微分di^2/dfとポロイダル電流の計算
    di2, polcur = get_di2_i_norm(cond, g.nr)
    cond['diff_i2_norm'] = di2
    cond['pol_current_norm'] = polcur
      
# 平衡計算の後処理
def equi_post_process(cond, verbose=3):
    
    # 形状パラメータの計算（elongationなど）
    set_domain_params(cond)
    if -1 == cond['cal_result']:
        display_information(cond, verbose=verbose, proc = 'post-process')
        return cond
    
    # 正規化フラックスの計算
    # 最期、ドメインの探索をして終わっているので、
    # まれにドメインと実際がずれているときがある。
    # 従って、再度normalized_fluxを計算する必要がある。
    dm_nfl = get_normalized_flux(cond)
    cond["flux_normalized"] = dm_nfl
    
    cond['boundary_plasma'] = find_lcfs(cond)
    cond['boundary_flux95'] = find_mag_surf_95(cond)
    set_domain_params_95(cond)
    
    # プラズマ圧力とポロイダル電流
    equi_post_proc_pressure_and_pol_current(cond)
    
    # 磁場に関する計算
    mag = smc.Magnetic(cond)

    # プラズマ電流によるフラックス,br, bz
    cond["flux_plasma"] = mag.fl_p
    cond["br_plasma"] = mag.br_p
    cond["bz_plasma"] = mag.bz_p
    cond["bt_plasma"] = mag.bt_p

    cond['br'] = mag.br
    cond['bz'] = mag.bz
    cond['bt'] = mag.bt
    
    # fluxループの位置におけるフラックスの計算    
    if 'fl_pos' in cond.keys():
        pos = cond['fl_pos']
        cond['fl_val'] = {}
        for k in pos.keys():
            r, z = pos[k]
            cond['fl_val'][k] = mag.get_fl(r, z)
    
    # 与えられた位置におけるBrの計算
    if 'br_pos' in cond.keys():
        pos = cond['br_pos']
        cond['br_val'] = {}
        for k in pos.keys():
            r, z = pos[k]
            cond['br_val'][k] = mag.get_br(r, z)
    
    # 与えられた位置におけるBzの計算
    if 'bz_pos' in cond.keys():
        pos = cond['bz_pos']
        cond['bz_val'] = {}
        for k in pos.keys():
            r, z = pos[k]
            cond['bz_val'][k] = mag.get_bz(r, z)

    # 与えられた位置におけるプラズマ由来Bzの計算
    if 'bz_pos' in cond.keys():
        pos = cond['bz_pos']
        cond['bz_val_plasma'] = {}
        for k in pos.keys():
            r, z = pos[k]
            cond['bz_val_plasma'][k] = mag.get_bz_plasma(r, z)
            
    # decay index on magnetic acis
    cond['decay_index_on_axis'] = mag.get_decay_index(cond['axis_r'], cond['axis_z'])

    # inductanceの計算
    cond = calc_inductance(cond)
    
    cond = calc_beta(cond)  # ベータ値の計算
    
    # safety factorの計算
    #cond = calc_safety(cond)  # 微分を取って補間
    #cond = calc_safety_poly(cond)  # 多項式近似した後に微分
    cond = calc_safety_poly2(cond)  # 微分後に多項式近似
    cond['q_center'] = cond['safety_factor_norm'][0]
    cond['q_edge'] = cond['safety_factor_norm'][-1]
    
    # ダイバータ配位の場合はストライク点, x点を探索
    if cond['conf_div'] == 1:
        search_strike_points(cond)
        search_x_points(cond)
        cond['boundary_x'] = find_x_boundary(cond)
    
    # 拘束条件の再現度
    if 'constraints' in cond.keys():
        dic = cond['constraints']
        for e in dic.keys():
            if 'ip' in dic[e].keys():
                dic[e]['ip_calc'] = cond['cur_ip']['ip']
            
            if 'stored_energy' in dic[e].keys():
                dic[e]["se_calc"] = cond['stored_energy']
            
            if 'point' in dic[e].keys():
                if dic[e]['point'] == 'axis':
                    rp, zp = cond['axis_r'], cond['axis_z']
                else:
                    rp, zp = dic[e]['point']
                if 'pressure' in dic[e].keys():
                    dic[e]['pressure_calc'] = emat.linval2(rp, zp, cond['pressure'])
                if 'jt' in dic[e].keys():
                    dic[e]['jt_calc'] = emat.linval2(rp, zp, cond['jt'])
                if 'flux' in dic[e].keys():
                    dic[e]['flux_calc'] = emat.linval2(rp, zp, cond['flux'])
                if 'br' in dic[e].keys():
                    dic[e]['br_calc'] = emat.linval2(rp, zp, cond['br'])
                if 'bz' in dic[e].keys():
                    dic[e]['bz_calc'] = emat.linval2(rp, zp, cond['bz'])                            
         
    # 計算結果の確認
    # 圧力分布の正負の確認
    a = [e <0 for e in cond["pressure_norm"]]
    if any(a):
        cond["cal_result"] = -1
        cond['error_messages'] += "Negtive pressure found.\n"
    
    # 正常終了の場合
    if cond["cal_result"] != -1:
        cond["cal_result"] = +1

    display_information(cond, verbose=verbose, proc='post-process')
    return cond

# 平衡計算(１回)
def equi_fit_and_evaluate_error(condition):
    cond = copy.deepcopy(condition)
    
    # 正規化フラックスの作成と保存
    dm_nf = get_normalized_flux(cond) # 正規化flux
    cond["flux_normalized"] = dm_nf
    # 1メッシュの面積を計算
    #ds = g.dr*g.dz

    # make_initial_matrix returns: 
    # a[num of plasma points, num of coef]
    # j[num of plasma points]
    a, j = make_initial_matrix(cond) 
    if -1 == cond['cal_result']:
        return cond
    
    jtotal = np.sum(j)  # 全電流を保持しておく

    # a1, v1, w1: including other constrains (ex. pressure)
    a1 = a.copy()
    v1 = j.copy() # value v[point数]
    w1 = np.array([1.0]*len(v1)) # weighting factor w[point数]

    # 束縛条件の追加
    a1, v1, w1 = constraints_procedure(cond, a1, v1, w1, a, j)

    w1 = np.diag(w1) # weighting matrixの作成

    # 重み付きの式の時、下の式を満たすcを求めればよい。
    # A_t W A c = (W A)_t v0 
    # A[nt, nc] (nt:ポイント数、nc:パラメータ数)
    # w[nt, nt]
    # v[nt]
    # フィッティングする場合は電流密度の値を用いる。
    m0 = np.dot(a1.transpose(), w1) # [np, nc]
    m0 = np.dot(m0, a1) # [np, np]
    #m1 = np.dot(a.transpose(), j/ds)
    m1 = np.dot(w1, a1).transpose() # [np, nc]
    m1 = np.dot(m1, v1) # jtを電流密度とする場合 [np]

    # m0にほんのわずかな値を加算してsingular matrixになるのを避ける
    # dd = np.min(np.abs(m0))*10**(-7)
    # m0 += np.identity(npr+ncu)*dd

    # もしsingular matrixになったらそこで計算終了
    if np.linalg.det(m0) == 0:
        cond['cal_result'] = -1
        cond['error_messages'] += 'Singular matrix occurs.\n'
        return cond
    params = np.dot(np.linalg.inv(m0), m1)
    
    j0 = np.dot(a, params)  # 新しい電流
    jsum = np.sum(j0)
    
    if jsum == 0.0:
        cond['cal_result'] = -1
        cond['error_messages'] += 'Sum of jt become zero.\n'
        return cond

    # 束縛条件の評価項：(1/2)w^2(v0-v1)^2
    # (1/2)w^2(v0-v1)^2 = (1/2)np.dot(w1, (np.dot(a1, params)-v1)**2)
    #eval_weight = (1/2)*np.dot(w1, (np.dot(a1, params)-v1)**2)
    #cond["eval_weight"] = eval_weight[len(j0):] # 束縛条件の所のみ抜き出す。
    
    # 次数の取得
    npr = cond["num_dpr"]

    # トータルの電流量の調整
    # この時点で、１メッシュ内に流れるトータルの電流に正規化される。
    #j0 *= jtotal / jsum  # トータルの電流値が維持されるように調整   
    
    # a) 一旦、トータルの電流値が維持されるように調整
    jcoef = jtotal / jsum
    
    j0 *= jcoef
    cond['jt'] = conv_1d_array_to_dmat(cond, j0)
    
    # 新しい圧力由来電流分布の作成と保存
    j0_p = np.dot(a[:, 0:npr], params[0:npr]) * jcoef
    cond['jt_dp'] = conv_1d_array_to_dmat(cond, j0_p)
            
    # 新しいポロイダル電流由来電流分布の作成と保存
    j0_d = np.dot(a[:, npr:], params[npr:]) * jcoef
    cond['jt_di2'] = conv_1d_array_to_dmat(cond, j0_d)
    
    # b) 次にプラズマ電流値が固定でない場合の処理
    # fix_curが存在して、かつFalseの時にトータル電流量が変化する。
    if ('fix_cur' in cond) and not cond['fix_cur']:
        # トータルの電流量が変動する場合の処理
        # 変動幅はclipで抑えられている。
        # np.clip(jsum/jtotal, 0.9, 1.1)で10%の変動を許容
        jcoef2 = np.clip(jsum/jtotal, 0.1, 1.9)
        j0 *= jcoef2
        j0_p *= jcoef2
        j0_d *= jcoef2
        
        cond['jt'] = conv_1d_array_to_dmat(cond, j0)
        cond['jt_dp'] = conv_1d_array_to_dmat(cond, j0_p)
        cond['jt_di2'] = conv_1d_array_to_dmat(cond, j0_d)

        cond['cur_ip']['ip'] = np.sum(j0)*(gl.cdel_r * gl.cdel_z)
                
        #print(f"jtotal: {jtotal/1e+6}, jsum: {jsum/1e+6}, jcoef2: {jcoef2}")

    # エラー値の算出_エラーの評価方法の幾つか    
    # 1. 単純な二乗残差
    #   domainが大きい場合はエラーが大きくなる？
    #   errest = np.sum((j0 - j) ** 2) / 2
    # 2. 二乗残差の平均(メッシュ当たりのエラー）
    #   domainの大きさに依存しないが、電流の大小に影響を受ける
    #   errest = np.average((j0 - j) ** 2) / 2
    # 3. メッシュ当たりのパーセンテージ絶対誤差の平均のようなもの 
    #   domain, 電流の値に依存しない
    #   errest = np.average(np.abs(j0-j))/np.average(np.abs(j))
    # 4. メッシュの絶対誤差の最大値とメッシュの平均値の比
    errest = np.max(np.abs(j0-j))/np.average(np.abs(j))
    
    cond["error"].append(errest)
    cond["param_dp"] = params[0:npr]
    cond["param_di2"] = params[npr:]
    
    return cond

def equi_calc_one_step(condition, verbose=3):
    cond = copy.deepcopy(condition)
    
    if -1 == cond['cal_result']:
        display_information(cond)
        return cond
    
    # プラズマ平衡
    cond = equi_fit_and_evaluate_error(cond)
    if -1 == cond['cal_result']:
        display_information(cond, verbose=verbose, proc='in-process')
        return cond
    
    # プラズマ電流のトリミング
    # ip>0なら全ての領域でjt>0となるようにする。
    dm_jt2 = pmat.trim_plasma_current(cond)
    cond["jt"] = dm_jt2

    # 磁気軸位置をプラズマ初期位置に一致させる。
    if ('fix_pos' in cond) and cond['fix_pos']:
        dm_jt = pmat.shift_plasma_profile(cond)
        cond["jt"] = dm_jt

    # iteration回数の設定
    cond["iter"] = len(cond["error"])
    # 固定境界の有無
    if ('fix_boundary' in cond.keys()) and cond['iter'] > 2:
        # PFコイルの調節
        fix_boundary_adjust_pf(cond)
        
        # 磁場クラスの作成
        mag = smc.Magnetic_flux(cond)

        cond["flux_coil"] = mag.fl_c
        cond["flux_plasma"] = mag.fl_p
        cond["flux"] = mag.fl

        #print('cur_pf: ', cond['cur_pf'])

    else:
        cond["flux_plasma"] = pmat.cal_plasma_flux(cond["jt"])
        cond["flux"] = emat.dm_add(cond["flux_plasma"], cond["flux_coil"])


    # 最外殻磁気面の探索
    dm_dm = search_domain(cond)
    cond["domain"] = dm_dm

    # エラー値の確認
    err = cond["error"]
    # 前回よりエラーが増えていたらbat_stepを+1
    if (len(err) >= 2) and (err[-1] > err[-2]):
        cond['bad_step'] += 1
    
    # iterationで監視すべき値があれば記録する
    check_monitor_value(cond)
            
    if 2 <= verbose:
        st = f'{len(err)} loss: {err[-1]:.4f}'
        st += conv_monitor_value_to_str(cond)
        
        print(st)
    
    display_information(cond, verbose=verbose)
    
    return cond

def display_information(cond, verbose=3, proc=''):
    # verbose: 
    #   0:None, 
    #   1:error
    #   2:error + character info
    #   3:error + character info + plot
    # proc: pre-process, in-process, post-process
    if 2 <= verbose:        
        if 0 != len(proc):
            print(proc)

    if 1 <= verbose:
        if -1 == cond['cal_result']:
            print(cond['error_messages'])
    
    if 3 <= verbose:
        name1 = 'jt'
        if 'pressure' in cond.keys():
            name1 = 'pressure'
        pl.contour_plasma(cond, name1)
        
    if 2 <= verbose and proc == 'post-process' and -1 != cond['cal_result']:
        print(f"major: {cond['major_radius']:.3f}, minor: {cond['minor_radius']:.3f}")
        print(f"major_95: {cond['major_radius_95']:.3f}, minor: {cond['minor_radius_95']:.3f}")        
        print(f"aspect: {cond['aspect_ratio']:.3f}, elongation: {cond['elongation']:.3f}")
        print(f"aspect_95: {cond['aspect_ratio_95']:.3f}, elongation_95: {cond['elongation_95']:.3f}")
        print(f"<P>v [kPa]: {cond['pressure_average_volume']/1000:.2f}, Paxis [kPa]: {cond['pressure_norm'][0]/1000:.2f}")
        print(f"beta_toroidal(%): {cond['beta_toroidal']*100:.3f}, beta_poloidal: {cond['beta_poloidal']:.3f}")
        d = cond['cur_pf']
        print({e:round(d[e]) for e in d.keys()})        
 
# 平衡計算
def calc_equilibrium(condition, iteration=100, verbose=3):
    # iteration: イタレーション数。
    #   指定があった場合は、最後までイタレーションする。
    # verbose: 1:詳細表示, 0:なし

    cond = copy.deepcopy(condition)
    cond = equi_pre_process(cond, verbose=2 if verbose == 3 else verbose)
    if cond["cal_result"] == -1:
        return cond

    for i in range(iteration):
        
        cond = equi_calc_one_step(cond, verbose=2 if verbose == 3 else verbose)
        if cond["cal_result"] == -1:
            return cond
    
        # 少なくとも２回は計算を行う。
        err = cond["error"]
        if len(err) <= 2:
            continue
        
        # iterationがデフォルト値でない場合は、設定されたという事。
        # このときは、最後までiterationする。
        if iteration < 100:
            continue

        # 2回連続して、前回値よりエラー値が大きくなったら終了
        # その時の配位がリミター配位なら収束しなかったとみなす。
        #if (err[-1] > err[-2]) and (err[-2] > err[-3]):
        #    if 0 == cond["conf_div"]:
        #        cond['error_messages'] += 'Error value increased in limiter configuration.\n'
        #        cond['cal_result'] = -1
        #    break

        # エラー値が悪くなった回数が規定に達したら、収束せず振動しているとみなす。
        if cond['bad_step'] > cond['bad_step_max']:
            cond["error_messages"] += "Calculation doesn't converge.\n"
            cond["cal_result"] = -1
            break
        
        # 一番最初の変化量に対して、最新の変化量が十分小さければ終了
        #v = np.abs((err[-1] - err[-2]) / (err[1] - err[0]))
        #if v < 10 ** (-5):
        #    break

            
        # error値が、ある値より小さくなったら終了
        if err[-1] < cond['conv_judge_val']:
            break
        
    cond = equi_post_process(cond, verbose=verbose)

    return cond
