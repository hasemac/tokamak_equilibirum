# 平衡計算の過程では使用しないが、定義しておくと便利な関数群

import numpy as np
import sub.functions as fnc
import sub.emat as emat

# 多項式分布の作成
def calc_polynomial_profile(cond, degree=2, peak_value=None, vol_ave=None):
    """1-x**degree のプロファイルを作成する。
    domain内の正規化フラックスを基に多項式 1-x**degree のプロファイルを作成する。
    温度分布や密度分布を作成したいときに使用する。
    磁気軸 x=0 で 1, 最外殻 x=1 で 0。
    Args:
        cond (dict): 計算条件
        degree (int, optional): 多項式の次数. Defaults to 2.
        peak_value (float, optional): 磁気軸での値の指定. Defaults to None.
        vol_ave (float, optional): 体積平均での値の指定. Defaults to None.

    Returns:
        dm_pmat: 分布が格納されたdmat
        parr: 正規化フラックスでの分布の配列
    """
    # domain内の正規化フラックスを基に多項式 1-x**degree のプロファイルを作成する。
    dm_domain = cond["domain"]
    dm_norflx = cond["flux_normalized"]

    d = dm_domain["matrix"].reshape(-1)    
    f = dm_norflx["matrix"].reshape(-1)
    
    g = emat.dm_array(dm_domain)
    nr, nz = g.nr, g.nz
    
    # 最外殻磁気面内のみ取り出す。
    ir = g.ir[d == 1]
    iz = g.iz[d == 1]
    f = f[d == 1]
    
    # 多項式分布の作成
    pmat = np.zeros((nz, nr))
    for v, i, j in zip(f, ir, iz):
        pmat[j, i] = 1-v**degree
    # 多項式の配列
    parr = [1-e**degree for e in np.linspace(0.0, 1.0, g.nr)]
    parr = np.array(parr)
    
    # dmatの作成
    dm_pmat = emat.get_dmat_dim(dm_domain)
    dm_pmat['matrix'] = pmat

    # 値の調整
    if peak_value is not None:
        dm_pmat['matrix'] *= peak_value
        parr *= peak_value
    elif vol_ave is not None:
        v0 = fnc.get_volume_average(dm_pmat, dm_domain)
        dm_pmat['matrix'] *= vol_ave/v0
        parr *= vol_ave/v0
    
    return dm_pmat, parr

# 温度分布の作成
def calc_temperature_profile(cond, dm_density):
    # kb = 1.38e-23 # boltzmann const
    # temp = pressure/density/kb # [K]
    # # 1 eV = kb T
    # temp = temp/11609 # 1 eV = 11609 K (=kb T)    
    dm_pressure = cond['pressure']
    dm_temp = emat.get_dmat_dim(dm_pressure)

    pr = dm_pressure['matrix'].reshape(-1)
    de = dm_density['matrix'].reshape(-1)
    
    g = emat.dm_array(dm_pressure)
    nr, nz = g.nr, g.nz
    ir = g.ir[de != 0.0]
    iz = g.iz[de != 0.0]
    pr = pr[de != 0.0]
    de = de[de != 0.0]

    pmat = np.zeros((nz, nr))
    for p, d, i, j in zip(pr, de, ir, iz):
        pmat[j, i] = p/d

    e = 1.6e-19
    dm_temp['matrix'] = pmat/e
    return dm_temp

# 正規化フラックスでの温度分布作成
def calc_temperature_array(cond, arr_norm_dens):
    """正規化フラックスでの密度分布の配列から、正規化フラックスでの温度分布を返す。

    Args:
        cond (dict): 計算条件
        arr_norm_dens (array): 正規化フラックスでの密度分布の配列

    Returns:
        array: 正規化フラックスでの温度分布
    """
    if len(cond['pressure_norm']) != len(arr_norm_dens):
        # データの長さが違うのでゼロを返す。
        return np.zeros(len(arr_norm_dens))
    iter = zip(cond['pressure_norm'], arr_norm_dens)
    arr = [p/d if d != 0.0 else 0.0 for p, d in iter]
    e = 1.6e-19
    return np.array(arr)/e

# 温度の計算
def calc_temp(pressure, density):
    """calculate temperature

    Args:
        pressure (float): [Pa]
        density (float): [m^(-3)]

    Returns:
        float: [eV]
    """
    # kb = 1.38e-23 # boltzmann const
    # temp = pressure/density/kb # [K]
    # # 1 eV = kb T
    # temp = temp/11609 # 1 eV = 11609 K (=kb T)
    
    e = 1.6e-19
    temp = pressure/density/e
    return temp

# 形状パラメータから座標を計算する関数
def cal_surface_points(num, r_major, r_minor, elongation, triangularity):
    """形状パラメータから座標を計算する関数

    Args:
        num (int): 生成する座標の個数
        r_major (float): [m], major radius
        r_minor (float): [m], minor radius
        elongation (float): elongation
        triangularity (float): tiaungularity

    Returns:
        (list, list): (list of r, list of z)
    """
    theta = np.linspace(0, 2*np.pi, num)
    r = r_major + r_minor*np.cos(theta) - r_minor*triangularity*(np.sin(theta)**2)
    z = r_minor*np.sin(theta) * elongation
    return r, z

# Scaling law
def taue_ITER89P(ip, r, a, k, n20, b, p, m=1):
    """ITER89P energy confinement time scaling law

    Args:
        ip (float): [MA] plasma current
        r (float): [m] major radius
        a (float): [m] minor radius
        k (float): elongation
        n20 (float): [1.0e20 m^(-3)] density
        b (float): [T] toroidal field
        p (float): [MW] input power
        m (float): [AMU] atomic mass unit, H=1, D=2, T=3

    Returns:
        float: [sec] energy confinement time
    """
    taue = 0.048 * m**0.5 * ip**0.85 * r**1.2 * a**0.3 * k**0.5 * n20**0.1 * b**0.2 * p**(-0.5)
    return taue

def taue_IPB98y2(ip, b, p, n19, r, e, k, m=1):
    """IPB98y2 energy confinement time scaling law

    Args:
        ip (float): [MA] plasma current
        b (float): [T] toroidal field
        p (float): [MW] input power
        n19 (float): [1.0e19 m^(-3)] density
        r (float): [m] major radius
        e (float): inverse aspect ratio
        k (float): elongation
        m (float): [AMU] atomic mass unit, H=1, D=2, T=3

    Returns:
        float: [sec] energy confinement time
    """
    taue = 0.0562 * m**0.19 * ip**0.93 * b**0.15 * p**(-0.69) * n19**0.41 * r**1.97 * e**0.58 * k**0.78
    return taue

def taue_globus(ip, b, p, n19, r, k):
    """GLOBUS energy confinement time scaling law

    Args:
        ip (float): [MA] plasma current
        b (float): [T] toroidal field
        p (float): [MW] input power
        n19 (float): [1.0e19 m^(-3)] density
        r (float): [m] major radius
        k (float): elongation

    Returns:
        float: [sec] energy confinement time
    """
    taue = 0.066 * ip**0.53 * b**1.05 * p**(-0.58) * n19**0.65 * r**2.66 * k**0.78
    return taue